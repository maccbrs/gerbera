<?php $asset = URL::asset('/'); ?> 
@extends('gerbera.master2')

@section('title', 'Board list')

@section('header-scripts')
 <link rel="stylesheet" type="text/css" href="{{$asset}}timepicker2/jquery.datetimepicker.css"/>
@endsection

@section('content')
 <div class="right_col" role="main">

  <div class="row">

    <div class="col-md-8">

      <div class="x_panel ">

        <div class="x_title">
          <h2>Result</h2>
          <div class="clearfix"></div>
        </div>
        
        <div class="x_content">
          <br />

          <table class="table table-bordered">
            <thead>
              <tr>
                <th>Agent</th>
                <th>Campaign</th>
                <th>Login</th>
                <th>Logout</th>
                <th>Break</th>
                <th>Lunch</th>
                <th>Meeting</th> 
                <th>Other</th>
              </tr>
            </thead>
            <tbody>
              @foreach($rows as $row)
              <tr>
                <th scope="row">{{$row->user['name']}}</th>
                <th scope="row">{{$row->campaign->campaign_id}}</th>
                <td>
                  @if($row->login)
                    {{$carbon->createFromFormat('Y-m-d H:i:s',$row->login)->format('M d, Y H:i:s')}}
                  @endif
                </td>
                <td>
                  @if($row->logout)
                    {{$carbon->createFromFormat('Y-m-d H:i:s',$row->logout)->format('M d, Y H:i:s')}}
                  @endif
                </td>
                <td>{{$row->break}}</td>
                <td>{{$row->lunch}}</td>
                <td>{{$row->meeting}}</td>
                <td>{{$row->other}}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>

    </div>
    <div class="col-md-4">
          {!!view('gerbera.reports.generate-form',compact('campaigns'))!!}
    </div>

  </div>

</div>
@endsection 

@section('footer-scripts')
  <script src="{{$asset}}timepicker2/build/jquery.datetimepicker.full.js"></script>
  <script type="text/javascript">
      var date = $('.datetimepicker').datetimepicker({
        timeFormat: 'Y-m-d HH:mm:ss'
      });
  </script>
@endsection