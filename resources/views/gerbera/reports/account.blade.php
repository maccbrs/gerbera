<?php $asset = URL::asset('/'); ?> 
@extends('gerbera.master2')

@section('title', 'Board list')

@section('header-scripts')
 <link rel="stylesheet" type="text/css" href="{{$asset}}timepicker2/jquery.datetimepicker.css"/>
@endsection

@section('content')
 <div class="right_col" role="main">
  <div class="row">
    <div class="col-md-12">
      <div class="x_panel ">
    <div class="x_title">
        <h2>Generate Report</h2>
        <div class="clearfix"></div>
    </div>

    <div class="x_content">
        <br />
        <form class="form-horizontal" role="form" method="POST" action="{{route('gerbera.reports.generate_account')}}">

            {{ csrf_field() }}
            <div class="col-md-4">
                <div class="form-group">

                    <input type="text" class="form-control datetimepicker" placeholder="From" name="from">
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">

                    <input type="text" class="form-control datetimepicker" placeholder="To" name="to">
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <button type="button" style = "width:100%" class="btn btn-primary " data-toggle="modal" data-target="#myModal"><i class="fa fa-file-text" aria-hidden="true"></i>  Generate
                    </button></h1>
                </div>
            </div>

            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="modal-title" id="myModalLabel">Campaign List</h3>
                  </div>
                  <div class="modal-body">
                      <input type="hidden" name="_method" value="POST">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}"> 

                        <div class="form-group row">
                          <div class="col-sm-12"></div> 
                            <div>
                                @foreach($campaigns as $c)
                                  <?php $campaign = json_decode($c); ?>
                                      <div class="col-md-3" style = "text-align:left;">
                                          <div class="radio" >
                                              <label style ="color:black">
                                                  <input  type="checkbox" name="campaign[]" value="{{$campaign->id}}" >{{$campaign->name}}
                                              </label>
                                          </div>                                               
                                      </div>
                                @endforeach   
                            </div>
                        </div>  

                        <div class="modal-footer">
                          <button type="submit" class="btn btn-primary" style="width:100px;">Filter now</button>
                        </div>
                      
                  </div>
                </div>
              </div>
            </div>

        </form>
        <div class="ln_solid"></div>
    </div>
</div>
    </div>

  </div>
</div>
@endsection 

@section('footer-scripts')
  <script src="{{$asset}}timepicker2/build/jquery.datetimepicker.full.js"></script>
  <script type="text/javascript">
      var date = $('.datetimepicker').datetimepicker({
        timeFormat: 'Y-m-d HH:mm:ss'
      });
  </script>
@endsection