<?php $asset = URL::asset('/'); ?> 
@extends('gerbera.master2')

@section('title', 'Board list')

@section('header-scripts')
 <link rel="stylesheet" type="text/css" href="{{$asset}}timepicker2/jquery.datetimepicker.css"/>
@endsection

@section('content')
 <div class="right_col" role="main">

  <div class="row">

    <div class="col-md-8">

      <div class="x_panel ">

        <div class="x_title">
          <h2>Result</h2>
          <div class="clearfix"></div>
        </div>
        
        <div class="x_content">
          <br />

          <table class="table table-bordered">
            <thead>
              <tr>
                <th>Agent</th>
                <th>Done</th>
                <th>Skipped</th>
                <th>Done%</th>
                <th>Skipped%</th>
                <th>Total</th>
                <th>Minutes Spent (pause deducted)</th>
              </tr>
            </thead>
            <tbody>
              @foreach($rows as $k => $v)

              <?php 
                  $done = $gn->catcher(isset($v['done']));
                  $skipped = $gn->catcher(isset($v['skipped']));
                  $total = $done+$skipped;
                  $time_spent = $gn->catcher(isset($v['time_spent']));
              ?>

              <tr>
                <th scope="row">{{$k}}</th>
                <td>{{$done}}</td>
                <td>{{$skipped}}</td>
                <td>
                  {{($total?round(($done/$total)*100,1):0)}}
                </td>
                <td>
                  {{($total?round(($skipped/$total)*100,1):0)}}
                </td>
                <td>{{$total}}</td>
                <td>{{round($time_spent/60,1)}}</td>
              </tr>
              @endforeach
            </tbody>
          </table>

        </div>
      </div>

    </div>
    <div class="col-md-4">
       {!!view('gerbera.reports.generate-form')!!}
    </div>

  </div>

</div>
@endsection 

@section('footer-scripts')
  <script src="{{$asset}}timepicker2/build/jquery.datetimepicker.full.js"></script>
  <script type="text/javascript">
      var date = $('.datetimepicker').datetimepicker({
        timeFormat: 'Y-m-d HH:mm:ss'
      });
  </script>
@endsection