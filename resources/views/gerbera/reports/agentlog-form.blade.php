  <div class="x_panel ">
    <div class="x_title">
      <h2>Agent Log Report</h2>
      <div class="clearfix"></div>
    </div>
  
    <div class="x_content">
      <br />
      <form class="form-horizontal" role="form" method="POST" action="{{route('gerbera.reports.generate_agent')}}">
        {{ csrf_field() }}

        <div class="form-group">
          <label class="control-label col-md-2" for="first-name">From<span class="required">*</span>
          </label>
          <div class="col-md-10">
            <input type="text" class="form-control datetimepicker" placeholder="From" name="from">
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-2" for="last-name">To<span class="required">*</span>
          </label>
          <div class="col-md-10">
            <input type="text" class="form-control datetimepicker" placeholder="To" name="to">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-2">Agent:</label>
          <div class="col-md-10">
            <select class="form-control" name="agent">
    
              @foreach($users as $user)
                <option value="<?php echo $user['id'] ?>"><?php echo $user['name'] ?></option> 
              @endforeach
            </select>
          </div>
        </div>
        <div class="ln_solid"></div>

        <div class="col-md-4">
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary" style="width:100px;">Filter now</button>
          </div>
        </div>

      </form>

    </div>
  </div>