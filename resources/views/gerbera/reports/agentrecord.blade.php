<table class="table table-bordered">
  <thead>
    <tr>
      <th>Login</th>
      <th>Break</th>
      <th>Lunch</th>
      <th>Meeting</th>
      <th>Other</th>
      <th>Logout</th>
      <th>Hours</th>
    </tr>
  </thead>
  <tbody>
    @foreach($rows as $row)
    <tr>
      <td>{{$row['login']}}</td>
      <td>{{$row['break']}}</td>
      <td>{{$row['lunch']}}</td>
      <td>{{$row['meeting']}}</td>
      <td>{{$row['other']}}</td>
      <td>{{$row['logged out']}}</td>
      <td>{{$row['hours']}}</td>
    </tr>              
    @endforeach
  </tbody>
</table>