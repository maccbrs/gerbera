<button type="button" class="btn btn-primary fa fa-plus pull-right" data-toggle="modal" data-target=".modalAddUser"></button>


  <div class="modal fade modalAddUser " tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">


              <div class="x_panel">
                <div class="x_title">
                  <h2>Add Campaign</h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                      <form class="form-horizontal form-label-left" method="post" action="{{route($route)}}" novalidate>
                         {{ csrf_field() }}
                            <div class="row">
                              <div class="col-md-10 col-md-offset-1">
                                {!!view('gerbera.forms.text',['label' => 'Name','name' => 'name','value' => ''])!!}
                                {!!view('gerbera.forms.text',['label' => 'Campaign Id','name' => 'campaign_id','value' => ''])!!}
                              </div>                     
                            </div>
                            <div class="form-group">
                              <div class="col-md-12">
                                <button  type="submit" class="btn btn-success pull-right">Save</button>
                              </div>
                            </div>
                      </form>
                </div>

          </div>

      </div>
    </div>
  </div>
