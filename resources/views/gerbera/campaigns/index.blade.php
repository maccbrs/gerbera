<?php $asset = URL::asset('/'); ?> 
@extends('gerbera.master2')

@section('title', 'dashboard')

@section('content')


      <div class="right_col" role="main">
        <div class="row">
          <div class="col-md-12" style="margin-top:30px">
            <div class="dashboard_graph">

              <div class="row x_title">
                <div class="col-md-6">
                  <h3>Campaigns</h3>
                </div>
                <div class="col-md-6">
                 {!!view('gerbera.campaigns.add-modal',['route' => 'gerbera.campaigns.create'])!!}
                </div>
              </div>
              <div class="col-md-12"> 

		              <div class="x_panel">
		                <div class="x_content">
			                  <table class="table table-striped responsive-utilities jambo_table bulk_action">
			                    <thead>
			                      <tr class="headings">
			                        <th class="column-title">Campaign id</th>
			                        <th class="column-title">Name</th>
			                        <th class="column-title">Created At</th>
			                      </tr>
			                    </thead>
			                    <tbody>
			                    	@if($items->count())
				                    	@foreach($items as $item)
				                    		<tr>
				                    			<td class="column-title">{{$item->campaign_id}}</td>
				                    			<td class="column-title">{{$item->name}}</td>
				                    			<td class="column-title">{{$item->created_at}}</td>
				                    		</tr>
				                    	@endforeach
			                    	@endif
			                    </tbody>
			                  </table>
		                </div>
		              </div>   


              </div>

              <div class="clearfix"></div>
            </div>
          </div>
        </div>
      </div>

@endsection 


@section('header-scripts')
@endsection

@section('footer-scripts')

@endsection