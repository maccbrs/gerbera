<?php $asset = URL::asset('/'); 
$manila_time = date("m-d-Y H:i");
date_default_timezone_set('America/Los_Angeles');
$pst_time = date('m-d-Y H:i');?> 
@extends('gerbera.master')

@section('title', 'Board list')

@section('header-scripts')
@endsection


@section('content')

<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Pacifico:400' rel='stylesheet' type='text/css'>

<script type="text/javascript" src="{{$asset}}gentella/js/simpletextrotater.js"></script>

    <link rel="stylesheet" href="{{$asset}}gentella/css/simpletextrotater.css">
    <script>
    
        $(document).ready(function(){
          $(".demo2 .rotate").textrotator({
            animation: "flip",
            speed: 1250
          });
        });
        
    </script>

    <div class = "row">
        <div class="col-xs-3"> </div>
        <div class="col-xs-6">

            <div class="main">
                <h1 class="demo2"><span class="rotate">Improved, Enhanced, Modernized </span><br> Real Time Application</h1>     
            </div>

            <br><br><br><br>
 
            <div class ="divbg">
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                    {{ csrf_field() }}
                    <br>
                    <h1>Login</h1>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} login">
                        <div class="col-md-12">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" style ="text-align:center;">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong> 
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <div class="col-md-12">
                            <input id="password" type="password" class="form-control" name="password" style ="text-align:center;">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <br>
                    <div>
                        <button type="submit" class="btn btn-rta-submit" >
                            <i class="fa fa-btn fa-sign-in"></i> Login
                        </button>
                    </div>
                    <br>
                    
                    <div class="clearfix"></div>
              </form>
            </div>
            <br>
             <div class="divbg">
                
                <div><h3><span class = "time_label"> PST Time : </span><span class = "time"> {{$pst_time}} </span></h3></div>
                <div><h3><span class = "time_label"> Manila Time : </span><span class = "time">  {{$manila_time}} </span></h3></div>
            </div>
        </div>

        <div class="col-xs-3">
        </div>
    </div>

    <script>
    
         setInterval(function() {
          window.location.reload();
        }, 20000); 

    </script>

@endsection 

@section('footer-scripts')

@endsection