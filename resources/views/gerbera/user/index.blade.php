<?php $asset = URL::asset('/'); ?> 
@extends('gerbera.master2')

@section('title', 'dashboard')

@section('content')

										


      <div class="right_col" role="main">
        <div class="row">
          <div class="col-md-12" style="margin-top:30px">
            <div class="dashboard_graph">

              <div class="row x_title">
                <div class="col-md-6">
                  <h3>Users</h3>
                </div>
                <div class="col-md-6">
                 {!!view('gerbera.user.add-modal',['route' => 'gerbera.user.create'])!!}
                </div>
              </div>
              <div class="col-md-12"> 

		              <div class="x_panel">
		                <div class="x_content">
			                  <table class="table table-striped datatable responsive-utilities jambo_table bulk_action">
			                    <thead>
			                      <tr class="headings">
			                        <th class="column-title">Name</th>
			                        <th class="column-title">Email</th>
			                        <th class="column-title">User Type</th>
			                        <th class="column-title">Password</th>
			                        <th class="column-title">#</th>
			                      </tr>
			                    </thead>
			                    <tbody>
			                      @foreach($users as $user)
			                      <tr class="even pointer">
			                        <td>{{$user->name}}</td>
			                        <td>{{$user->email}}</td>
			                        <td>{{($user->settings?$user->settings->user_type:'')}}</td>
			                        <td>{{($user->settings?$user->settings->password:'')}}</td>
			                        <td>
									
										<button type="button"  data-userid="{{$user->id}}" class="button3 btn btn-primary fa fa-flag pull-right" data-toggle="modal" data-target=".modalReset"></button>
										<button type="button"  data-userid="{{$user->id}}" class="button2 btn btn-primary fa fa-trash pull-right" data-toggle="modal" data-target=".modalDelete"></button>
			                        	<button type="button" data-userid="{{$user->id}}"  data-user_type="{{$user->user_type}}"   data-username="{{$user->name}}" data-useremail="{{$user->email}}" class="button1 btn btn-primary fa fa-edit pull-right" data-toggle="modal" data-target=".modalEditUser"></button>
			                 
			                        </td>
			                      </tr>
			                      @endforeach
			                    </tbody>
			                  </table>
		                </div>
		              </div>   


              </div>

              <div class="clearfix"></div>
            </div>
          </div>
        </div>
      </div>
	  
	  
	<div class="modal fade modalEditUser" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">


              <div class="x_panel">
                <div class="x_title">
                  <h2>Edit User</h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                      <form class="form-horizontal form-label-left" id="update_users_form" method="post" action="{{route('gerbera.user.update',0)}}" novalidate>
                         {{ csrf_field() }}
                            <div class="row">
                              <div class="col-md-10 col-md-offset-1">
                                {!!view('gerbera.forms.text',['label' => 'Name','name' => 'name','value' => ''])!!}
                                {!!view('gerbera.forms.text',['label' => 'Email','name' => 'email','value' => ''])!!}
                               
                                {!!view('gerbera.forms.dropdown',['label' => 'User type','name' => 'user_type','value' => $user->user_type,'choices' => ['admin' => 'admin','agent' => 'agent','operation' => 'operation']])!!}
                              
                              </div>                     
                            </div>
                            <div class="form-group">
                              <div class="col-md-12">
                                <button  type="submit" class="btn btn-success pull-right">Update</button>
                              </div>
                            </div>
                      </form>
                </div>

          </div>

      </div>
    </div>
  </div>
	  
	  
	<div class="modal fade modalDelete" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">

      <form class="form-horizontal form-label-left" id="delete_users_form"  method="post" action="0" novalidate>
        {{ csrf_field() }}
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
              </button>
              <h4 class="modal-title" id="myModalLabel">Are you sure you want to delete ?</h4>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-primary">Proceed</button>
            </div>
          </div>
      </form>

    </div>
  </div>
  
  
  <div class="modal fade modalReset" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">

      <form class="form-horizontal form-label-left" id="reset_users_form"  method="post" action="0" novalidate>
        {{ csrf_field() }}
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
              </button>
              <h4 class="modal-title" id="myModalLabel">Are you sure you want to Reset Password ?</h4>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-primary">Proceed</button>
            </div>
          </div>
      </form>

    </div>
  </div>
	  

@endsection 


@section('header-scripts')
@endsection

@section('footer-scripts')
<script>
	
$(document).ready( function () {
    $('.datatable').DataTable();
} );


$('.button1').click(function(){
   $('#update_users_form').attr('action', window.location.origin + '/user/update/' +  $(this).data('userid'));
   $("input[name='name']" ).val( $(this).data('username'));
   $("input[name='email']" ).val( $(this).data('useremail'));
  
   $(".sel_user_type" ).val( $(this).data('user_type'));
  
});


$('.button2').click(function(){
   $('#delete_users_form').attr('action', window.location.origin + '/user/destroy/' +  $(this).data('userid'));
  
  
});
$('.button3').click(function(){
   $('#reset_users_form').attr('action', window.location.origin + '/user/reset_pass/' +  $(this).data('userid'));
  
  
});
</script>

@endsection