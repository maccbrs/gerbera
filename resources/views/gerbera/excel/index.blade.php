<?php $asset = URL::asset('/'); ?> 
@extends('gerbera.master2')

@section('title', 'Board list')

@section('header-scripts')
@endsection

@section('content')
 <div class="right_col" role="main">
  <div class="row">
    <div class="col-md-8 ">

         <div class="x_panel">

            <div class="x_title">
               <h2>Leads</h2>
               <div class="clearfix"></div>
            </div>
            <div class="x_content">
               <table class="table">
                  <thead>
                     <tr>
                        <th>Upload date</th>
                        <th>Name</th>
                        <th>Uploader</th>
                        <th>updates</th>
                        <th></th>
                     </tr>
                  </thead>
                  <tbody>
                     @foreach($lists as $list)
                     <tr>
                        <th scope="row">{{$list->created_at}}</th>
                        <td>{{$list->name}}</td>
                        <td>{{$list->user->name}}</td>
                        <td>#</td>
                        <td>
                           <div class="btn-group">
                             <button type="button" class="btn btn-primary">#</button>
                             <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                               <span class="caret"></span>
                               <span class="sr-only">Toggle Dropdown</span>
                             </button>
                             <ul class="dropdown-menu" role="menu">
                               <li>
                                <a href="{{route('gerbera.excel.lists',$list->id)}}">view</a>
                                {!!view('gerbera.modals.unload-btn',['id' => $list->id,'label' => $list->name])!!}
                               </li>
                             </ul>
                           </div>
                        </td> 
                     </tr>
                     @endforeach
                  </tbody>
               </table>
            </div>
         </div>

    </div>
  </div>
</div>
   @foreach($lists as $list)
    {!!view('gerbera.modals.unload-body',['id' => $list->id,'label' => $list->name])!!}
   @endforeach
@endsection 

@section('footer-scripts')

@endsection
