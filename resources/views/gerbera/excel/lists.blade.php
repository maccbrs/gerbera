<?php $asset = URL::asset('/'); ?> 
@extends('gerbera.master2')

@section('title', 'Board list')

@section('header-scripts')
@endsection

@section('content')
 <div class="right_col" role="main">
  <div class="row">

      <div class="col-md-12 col-sm-12 col-xs-12">
         <div class="x_panel">
            <div class="x_title">
               <h2>Lead list</h2>
               <div class="clearfix"></div>
            </div>
            <div class="x_content">
               <div class="row">
                  <div class="col-sm-12">
                     <div class="card-box table-responsive">
                        <table id="datatable-keytable" class="table table-striped table-bordered">
                              <thead>
                                 <tr>
                                    @foreach($keys as $key)
                                    <th>{{$key}}</th>
                                    @endforeach
                                 </tr>
                              </thead>
                              <tbody>
                                 @foreach($data as $d)
                                 <tr>
                                    @foreach($d as $i)
                                    <th scope="row">{{$i}}</th>
                                    @endforeach
                                 </tr>
                                 @endforeach
                              </tbody>
                        </table>
                        {{$rows->links()}}
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>


  </div>
</div>
@endsection 

@section('footer-scripts')

@endsection
