<?php $asset = URL::asset('/'); ?> 
@extends('gerbera.master2')

@section('title', 'Board list')

@section('header-scripts')
@endsection

@section('content')
 <div class="right_col" role="main">
  <div class="row">
    <div class="col-md-8 ">

         <div class="x_panel">

            <div class="x_title">
               <h2>Archived</h2>
               <div class="clearfix"></div>
            </div>
            <div class="x_content">
               <table class="table">
                  <thead>
                     <tr>
                        <th>Created</th>
                        <th>filename</th>
                        <th></th>
                     </tr>
                  </thead>
                  <tbody>
                     @foreach($rows as $row)
                     <tr>
                        <th scope="row">{{$row->created_at}}</th>
                        <td>{{$row->name}}</td>
                        <td><a href="{{route('gerbera.excel.download',$row->filename)}}">Download</a></td>
                     </tr>
                     @endforeach
                  </tbody>
               </table>
            </div>
         </div>

    </div>
  </div>
</div>

@endsection 

@section('footer-scripts')

@endsection
