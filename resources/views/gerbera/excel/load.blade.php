<?php $asset = URL::asset('/'); ?> 
@extends('gerbera.master2')

@section('title', 'Board list')

@section('header-scripts')
@endsection

@section('content')
 <div class="right_col" role="main">
  <div class="row">
    <div class="col-md-6 col-md-offset-2">

        <div class="x_panel">

          <div class="x_title">
            <h2>Upload</h2>
            <div class="clearfix"></div>
          </div>

          <div class="x_content">
            <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="{{route('gerbera.excel.post')}}">
              {{ csrf_field() }}

              <label>Title:</label>
              <input type="text" class="form-control" name="title" required />

              <div class="clearfix"></div>
              <h2>file:</h2>
              <input type="file" name="excelfile" class="btn btn-primary">


                <button type="submit" class="btn btn-success pull-right">Submit</button>

              <div class="clearfix"></div>
            </form>
          </div>

        </div>

    </div>
  </div>
</div>
@endsection 

@section('footer-scripts')

@endsection