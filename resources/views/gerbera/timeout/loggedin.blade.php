<?php $asset = URL::asset('/'); ?> 
@extends('gerbera.master2')

@section('title', 'dashboard')

@section('content')

<?php 
$status = ['meeting' => 'meeting','lunch' => 'lunch','break' => 'break','other' => 'other'];
?>

<div class="right_col " role="main">
  <br><br><br>
    <div class="row">
          <div class="col-md-12">
            <div class="x_panel tile divbg">
              <div class="x_content">

                <h1 style="display: inline;">Current Status: <span id="current-session"></span></h1>
               
                <button type="button" class="btn btn-success pull-right btn-filter" data-toggle="modal" data-target=".modalSwitch">Switch</button>
                <button type="button" class="btn btn-success pull-right btn-filter" data-toggle="modal" data-target=".modalLogout">TimeOut</button>
              </div>
            </div>
          </div>
    </div>
    <div class="row">
          <div class="col-sm-4">
            <div class="x_panel tile divbg">
              <div class="x_content">

                @foreach($status as $k => $v)
                <div class="row">

                  <div class="col-md-4 div-btns">

                    <button type="button" class="btn btn-app {{$k}}-pause pause" onclick="return "  style='display:none; padding: 0px 0px;'>
                      <img src = <?php echo $asset ?>gentella/images/clock.gif  style="width:70%;height:100%;"> 
                    </button>

                    <button type="button" class="btn btn-app {{$k}}-resume resume" onclick="return " data-mtype="{{$v}}" style='padding: 0px 0px;' >
                       <img src = <?php echo $asset ?>gentella/images/clockrun.png  style="width:70%;height:100%;"> 
                    </button> 

                  </div>

                    <div class="col-md-4">
                      <div id="{{$k}}" class="mb-time">{{(isset($session[$k])?$session[$k]:'0:0:0')}}</div>
                    </div>  
                    <div class="col-md-4">
                      <h2>{{ucfirst($v)}}</h2>
                    </div>                  
                  <div class="clearfix"></div>
                </div>
                @endforeach
              </div>
            </div>
              
          </div>

          <div class="col-sm-8">
            <div class="x_panel tile divbg">
              <div class="x_title">
                <h1>Time Records  </h1>
                <div class="clearfix"></div>
              </div>

              <div class="x_content" id="agentrecord">


 
              </div>

            </div>
          </div>

    </div>
 </div>



  <div class="modal fade modalLogout" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">

      <form class="form-horizontal form-label-left" method="post" action="{{route('gerbera.status.timeout')}}" novalidate>
        {{ csrf_field() }}
           <input type="hidden" name="account_id" value="1">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
              </button>
              <h4 class="modal-title">Are you sure you want to time out?</h4>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-primary">Proceed</button>
            </div>
          </div>
      </form>

    </div>
  </div>

  <div class="modal fade modalSwitch" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">

      <form class="form-horizontal form-label-left" method="post" action="{{route('gerbera.campaign.switch')}}" novalidate>
        {{ csrf_field() }}
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
              </button>


                  <h4 class="modal-title">Choose campaign</h4>
                  <div class="form-group">
                    <input type="hidden" name="id" value="{{$log['id']}}">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Campaigns</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <select class="form-control" name="campaign_id" required>
                        <option value = ''>Choose option</option>
                        @foreach($campaigns as $v)
                           <option value="{{$v->id}}" >{{$v->name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>

            </div> 
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-primary">Proceed</button>
            </div>
          </div>
      </form>

    </div>
  </div>


  <div class="modal fade modalBacktowork" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">

      <form class="form-horizontal form-label-left" method="post" action="{{route('gerbera.timeout.backtowork')}}" novalidate>
        {{ csrf_field() }}
           <input type="hidden" name="account_id" value="1">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
              </button>
              <h4 class="modal-title">Back to work?</h4>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-primary">Proceed</button>
            </div>
          </div>
      </form>

    </div> 
  </div>


@endsection 


@section('header-scripts')
<style type="text/css">
	.mb-time{
		font-size: 25px;
	}
  #current-session{
    text-transform: capitalize;
  }
  .row < .clicked{
    background-color: #ebebeb;
  }
  .row < .div-btns{
    padding-top: 5px;
  }
  .ln{
    border-top: 1px solid #e5e5e5;
    margin: 5px 0;
    height: 1px;
  }

.disabled{
  pointer-events: none;
}
</style>
@endsection

@section('footer-scripts')
<script language="javascript" type="text/javascript" src="{{$asset}}gentella/js/timer.jquery.min.js"></script> 
<script language="javascript" type="text/javascript" src="{{$asset}}gentella/js/clock.js"></script>
	<script type="text/javascript">

    !function(a) {
        a.mb_lib = {
            mb_ajax: function(b) {
                var c = {
                    data: "null",
                    url: "null",
                    type: "post",
                    attachto: "null"
                };
                if (b) a.extend(c, b);
                a.ajax({
                    url: c["url"],
                    data: c["data"],
                    type: c["type"],
                    success: function(d) {
                    //  $(c['attachto']).html(d);
                    console.log(d);
                    },
                    error: function(a) {

                    }
                });
            }
        };      
    }(jQuery); 


    var mb_session = '{{$log["status"]}}';

    $('#current-session').text(mb_session);

    var base = "{{URL::to('/')}}";
		@foreach($status as $k => $v)
      @if($log['status'] == $v)
        $('#{{$k}}').timer({seconds: {{$gn->ttosec($log[$k])}},format: '%H:%M:%S'});
        $('.{{$k}}-resume').hide();
        $('.{{$k}}-pause').show();

      @else
        $('#{{$k}}').timer({seconds: {{$gn->ttosec($session[$k])}},format: '%H:%M:%S'});
        $('#{{$k}}').timer('pause'); 
      @endif   
    @endforeach
 
    @foreach($status as $k => $v)
  
      $('.{{$k}}-resume').on('click',function(){
        
        @foreach($status as $k2 => $v2)
          @if($k2 != $k) 
            if($('#{{$k2}}').data('seconds')){
              $('#{{$k2}}').timer('pause');
              $('.{{$k2}}-pause').hide();
              $('.{{$k2}}-resume').show();
            }         
          @endif
        @endforeach       
        $('#{{$k}}').timer('resume');
        var $this = $(this);
         console.log($this);
        $this.hide();
        $this.siblings().show();    
      });

    @endforeach    

    $('.resume').on('click',function(){
      var $this = $(this);
      var mb_session = $this.data('mtype');
      $('#current-session').text(mb_session);
    });

    $('.pause').on('click',function(e){
      if(e.which){
        $('.modalBacktowork').modal('show');     
      }
    });

		$(document).ready(function(){

		  $('#clock1').jsclock("{{$time}}");
 
		});		

    $(window).load(function() {

      var numItems = $('.resume:visible').length

      if(numItems != 4){

          $('.resume').prop("disabled", true);

      }

    });

    $('.div-btns').on('click',function(e){
      alert('hi');
      var $this = $(this);
        $('.resume').addClass('disabled'); 
        $('.div-btns').removeClass('clicked');
        $('.div-btns').addClass('inactive');
        $this.addClass('clicked');
        $this.removeClass('inactive');

        var data = {_token:"{{csrf_token()}}",user:"{{Auth::user()->id}}",status:$('#current-session').text(),log_id:$('.log_id').text()};

        @foreach($status as $k => $v)
            data["{{$k}}"] = "{{$session[$k]}}";
            if($('#{{$k}}').data('seconds')){
              data["{{$k}}"] = $('#{{$k}}').data('seconds');
            }else{
              data["{{$k}}"] = 0;
            }         
        @endforeach 

        if(e.which){
          var a = {
              data: data,
              url: base + "/status/update"
          };
          $.mb_lib.mb_ajax(a);

        }
      
    });

        $.mb_lib.mb_ajax({
                 data: { '_token':"{{csrf_token()}}"},
                 url: "{{route('gerbera.reports.agentrecord')}}",
                 type:'get',
                 attachto:"#agentrecord"
        });

	</script>
@endsection