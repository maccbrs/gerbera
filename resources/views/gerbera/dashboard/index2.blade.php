<?php $asset = URL::asset('/'); ?> 
@extends('gerbera.master2')

@section('title', 'Board list')

@section('header-scripts')

@endsection

@section('content')
 <div class="right_col" role="main">
  <div class="row">
    <div class="col-md-12">
      <div class="x_panel">
        <div class="x_title">
          
            <h1>Currently Logged In Agent/s <button type="button" class="btn btn-primary btn-lg  pull-right" data-toggle="modal" data-target="#myModal"><i class="fa fa-filter" aria-hidden="true"></i>  Filter
            </button></h1>

            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="modal-title" id="myModalLabel">Campaign List</h3>
                  </div>
                  <div class="modal-body">
                    <form method="post" action="{{route('gerbera.dashboard.filter')}}"> 
                      <input type="hidden" name="_method" value="POST">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                        <div class="form-group row">
                          <div class="col-sm-12"></div> 
                            <div>

                                @foreach($campaigns as $c)
                                  
                                  <?php $campaign = json_decode($c); ?>

                                  
                                      <div class="col-md-3">
                                          <div class="radio">
                                              <label style ="color:black">
                                                  <input  type="checkbox" name="campaign[]" value="{{$campaign->id}}" >{{$campaign->name}}
                                              </label>
                                          </div>                                                   
                                      </div>

                                   
                                @endforeach   
                            </div>
                        </div>  

                        <div class="modal-footer">
                          <button type="submit" class="btn btn-primary" style="width:100px;">Filter now</button>
                        </div>
                      </form>
                  </div>
                </div>
              </div>
            </div>
            
          <div class="clearfix"></div>

        </div>
        
        <div id="mb-agent-monitoring" class="x_content">
        <br />

        <img src = "{{$asset}}gentella/images/loading.gif" class = "loader_img"> <h1> Loading </h1>

        </div>
      </div>

    </div>


  </div>

</div>
@endsection 

@section('footer-scripts')
<script type="text/javascript">

    !function(a) {
        a.mb_lib = {
            mb_ajax: function(b) {
                var c = {
                    data: "null",
                    url: "null",
                    type: "post",
                    attachto: "null"
                };
                if (b) a.extend(c, b);
                a.ajax({
                    url: c["url"],
                    data: c["data"],
                    type: c["type"],
                    success: function(b) {
                      a(c["attachto"]).html(b);
                    },
                    error: function(a) {

                    }
                });
            },
            mbtest: function(a){
              console.log(a);
            }
        };      
    }(jQuery);

        var agentMonitoringRoute = "{{route('gerbera.monitoring.agent')}}";
        var token = "{{ csrf_token() }}";
        var a = {
            attachto: '#mb-agent-monitoring',
            data: {'_token':token},
            url: agentMonitoringRoute
        };
        

 setInterval(function(){$.mb_lib.mb_ajax(a);},5000);
</script>
@endsection