<?php $asset = URL::asset('/'); ?> 
@extends('gerbera.master2')

@section('title', 'dashboard')

@section('content')

<?php $disabled_button = isset($session[$log->status])?'true':'false'; ?>

<div class="right_col " role="main">
  <br><br><br>
    @if (Session::has('msg'))
      <div class="alert alert-error">{!!Session::get('msg')!!}</div>
    @endif
    <div class="row">
          <div class="col-md-12">
            <div class="x_panel tile divbg">
              <div class="x_content">

                <h1 style="display: inline;">Current Status: <span id="current-session">{{$log->status}}</span></h1>
               
                <button type="button" class="btn btn-success pull-right btn-filter" data-toggle="modal" data-target=".modalSwitch">Switch</button>
                <button type="button" class="btn btn-success pull-right btn-filter" data-toggle="modal" data-target=".modalLogout">TimeOut</button>
              </div>
            </div>
          </div>
    </div>
    <div class="row">
          <div class="col-sm-4">
            <div class="x_panel tile divbg">
              <div class="x_content">

                @foreach(['meeting' => 'meeting','lunch' => 'lunch','break' => 'break','other' => 'other'] as $key => $v)
                <div class="row">

                  <div class="col-md-4 div-btns">
                    @if($log->status == $key)
                      {!!view('gerbera.timekeeper.stop-modal',compact('asset','key','ip'))!!}
                    @else
                       {!!view('gerbera.timekeeper.run-modal',compact('asset','key','disabled_button','ip'))!!}
                    @endif
                  </div>

                    <div class="col-md-4">
                      <div id="{{$key}}" class="mb-time">{{(isset($session[$key])?$session[$key]:'0:0:0')}}</div>
                    </div>  
                    <div class="col-md-4">
                      <h2>{{ucfirst($v)}}</h2>
                    </div>                     
                  <div class="clearfix"></div>
                </div>
                @endforeach


              </div>
            </div>
              
          </div>

          <div class="col-sm-8">
            <div class="x_panel tile divbg">
              <div class="x_title">
                <h1>Time Records  </h1>
                <div class="clearfix"></div>
              </div>

              <div class="x_content" id="agentrecord">

               <table class="table">
                  <thead>
                     <tr>
                        <th>Login</th>
                        <th>Break</th>
                        <th>Lunch</th>
                        <th>Meeting</th>
                        <th>Other</th>
                        <th>Logout</th>
                        <th>Hours</th>
                     </tr>
                  </thead>
                  <tbody>
                     @foreach($logs as $item)
                     <tr>
                        <th >{{$item->login}}</th>
                        <th >{{$item->break}}</th>
                        <th >{{$item->lunch}}</th>
                        <th >{{$item->meeting}}</th>
                        <th >{{$item->other}}</th>
                        <th>{{$gn->haveLogOut($item)}}</th>
                        <th>{{$gn->getHoursDuty($item)}}</th>
                     </tr>
                     @endforeach
                  </tbody>
               </table>
 
              </div>

            </div>
          </div>

    </div>
 </div>



  <div class="modal fade modalLogout" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
      <form class="form-horizontal form-label-left" method="post" action="{{route('gerbera.timekeeper.timeout')}}" novalidate>
        {{ csrf_field() }}
           <input type="hidden" name="account_id" value="1">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
              </button>
              <h4 class="modal-title">Are you sure you want to time out?</h4>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-primary">Proceed</button>
            </div>
          </div>
      </form>
    </div>
  </div>

  <div class="modal fade modalSwitch" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">

      <form class="form-horizontal form-label-left" method="post" action="{{route('gerbera.timekeeper.switch')}}" novalidate>
        {{ csrf_field() }}
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
              </button>


                  <h4 class="modal-title">Choose campaign</h4>
                  <div class="form-group">
                    <input type="hidden" name="id" value="{{$log->id}}">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Campaigns</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <select class="form-control" name="campaign_id" required>
                        <option value = ''>Choose option</option>
                        @foreach($campaigns as $v)
                           <option value="{{$v->id}}" >{{$v->name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>

            </div> 
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-primary">Proceed</button>
            </div>
          </div>
      </form>

    </div>
  </div>


  <div class="modal fade modalBacktowork" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">

      <form class="form-horizontal form-label-left" method="post" action="{{route('gerbera.timeout.backtowork')}}" novalidate>
        {{ csrf_field() }}
           <input type="hidden" name="account_id" value="1">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
              </button>
              <h4 class="modal-title">Back to work?</h4>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-primary">Proceed</button>
            </div>
          </div>
      </form>

    </div> 
  </div>


@endsection 


@section('header-scripts')
<style type="text/css">
	.mb-time{
		font-size: 25px;
	}
  #current-session{
    text-transform: capitalize;
  }
  .row < .clicked{
    background-color: #ebebeb;
  }
  .row < .div-btns{
    padding-top: 5px;
  }
  .ln{
    border-top: 1px solid #e5e5e5;
    margin: 5px 0;
    height: 1px;
  }

.disabled{
  pointer-events: none;
}
</style>
@endsection

@section('footer-scripts')
<script language="javascript" type="text/javascript" src="{{$asset}}gentella/js/timer.jquery.min.js"></script> 
<script language="javascript" type="text/javascript" src="{{$asset}}gentella/js/clock.js"></script>
<script type="text/javascript">

    @if(isset($session[$log->status]))
    $('#{{$log->status}}').timer({seconds: {{$gn->ttosec($session[$log->status])}},format: '%H:%M:%S'});
    $('#{{$log->status}}').timer('resume');
    @endif

</script>
@endsection