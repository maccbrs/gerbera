<?php $asset = URL::asset('/'); ?> 
@extends('gerbera.master2')

@section('title', 'dashboard')

@section('content')

<?php 
$status = ['meeting' => 'meeting','lunch' => 'lunch','break' => 'break','other' => 'other'];
?>
      <!-- page content -->
      
       
      <div class="right_col" role="main">
        <div class="row">
          <div class="col-md-8 col-md-offset-2" style="margin-top:30px">
            <div class="dashboard_graph divbg">

              <div class="row x_title">
                <div class="col-md-6">
                  <h3>Currently Time Out</h3>
                </div>
                <div class="col-md-6">
                  <button type="button" class="btn btn-success pull-right btn-filter" data-toggle="modal" data-target=".modalLogin">TimeIn</button>
                </div>
              </div>
              <div class="col-md-12"> 
                show latest time records 
              </div>

              <div class="clearfix"></div>
            </div>
          </div>
        </div>
      </div>

      <!-- /page content -->

  <div class="modal fade modalLogin" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">

      <form class="form-horizontal form-label-left" method="post" action="{{route('gerbera.timekeeper.timein')}}" novalidate>
        {{ csrf_field() }}
           <input type="hidden" name="account_id" value="1">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
              </button>

                  <h4 class="modal-title">Choose campaign</h4>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Campaigns</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <select class="form-control" name="campaign_id" required>
                        <option value = ''>Choose option</option>
                        @foreach($campaigns as $v)
                           <option value="{{$v->id}}" >{{$v->name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-primary">Proceed</button>
            </div>
          </div>
      </form>

    </div>
  </div>



@endsection 


@section('header-scripts')
<style type="text/css">
	.mb-time{
		font-size: 25px;
	}
  #current-session{
    text-transform: capitalize;
  }
  .row < .clicked{
    background-color: #ebebeb;
  }
  .row < .div-btns{
    padding-top: 5px;
  }
  .ln{
    border-top: 1px solid #e5e5e5;
    margin: 5px 0;
    height: 1px;
  }
  .mb-tc-div span{
    padding-right: 7px;
    font-size: 11px;
    font-weight: bold;
    text-transform: uppercase;    
  }
  .mb-tc-div p{
    margin-bottom: 2px;
  }  
</style>
@endsection

@section('footer-scripts')
<script language="javascript" type="text/javascript" src="{{$asset}}gentella/js/timer.jquery.min.js"></script> 
<script language="javascript" type="text/javascript" src="{{$asset}}gentella/js/clock.js"></script>
  <script type="text/javascript">

    !function(a) {
        a.mb_lib = {
            mb_ajax: function(b) {
                var c = {
                    data: "null",
                    url: "null",
                    type: "post",
                    attachto: "null"
                };
                if (b) a.extend(c, b);
                a.ajax({
                    url: c["url"],
                    data: c["data"],
                    type: c["type"],
                    success: function(b) {
                      console.log(b);
                    },
                    error: function(a) {

                    }
                });
            }
        };      
    }(jQuery); 



    var base = "{{URL::to('/')}}";

 

   

    $('.div-btns').on('click',function(e){
      var $this = $(this);
      $('.div-btns').removeClass('clicked');
      $('.div-btns').addClass('inactive');
      $this.addClass('clicked');
      $this.removeClass('inactive');


      if(e.which){
        var a = {
            data: data,
            url: base + "/status/update"
        };
        $.mb_lib.mb_ajax(a);

      }

    });

  </script>
@endsection