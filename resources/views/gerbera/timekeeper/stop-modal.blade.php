<?php $mykey = substr(uniqid('', true), -5); ?>
<button type="button" class="btn btn-success pull-right btn-filter" data-toggle="modal" data-target=".modal-stop-{{$key}}"><img src="{{$asset.'gentella/images/clock.gif'}}" style="width:70%;height:100%;"></button>

<div class="modal fade modal-stop-{{$key}}" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <form class="form-horizontal form-label-left" method="post" action="{{route('gerbera.timekeeper.run')}}" novalidate>
      {{ csrf_field() }}
        <input type="hidden" name="createdkey" value="{{$mykey}}">
        <input type="hidden" name="ip" value="{{$ip}}">
        <input type="hidden" name="status" value="logged in">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title" style="color:#000">Please enter this key "{{$mykey}}" then press enter</h4>
            <div class="col-md-9">
                <div class="radio">
                    <label style ="color:black">
                        <input  type="text" name="enteredkey" value="" >
                    </label>
                </div>                                                   
            </div>
          </div>
        </div>
    </form>
  </div>
</div>