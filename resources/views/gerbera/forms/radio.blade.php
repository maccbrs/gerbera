<?php $value = (in_array($value, $choices)?$value:$default); ?>
<div class="item form-group">
  <label class="control-label col-md-3" for="name">{{$label}}
  </label>
  <div class="col-md-9">
      @foreach($choices as $k => $c)
      <div class="radio">
        <label>
          <input type="radio" class="flat" {{($value == $k?'checked':'')}} name="{{$name}}" value="{{$k}}"> {{$c}}
        </label>
      </div>
      @endforeach
  </div>
</div>






