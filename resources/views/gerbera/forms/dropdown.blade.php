<div class="form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12">{{$label}}</label>
  <div class="col-md-9 col-sm-9 col-xs-12">
    <select class="form-control sel_{{$name}}" name="{{$name}}">
      <option value = ''>Choose option</option>
      @foreach($choices as $k => $v)
      <option value="{{$k}}" {{($k == $value?'selected':'')}}>{{$v}}</option>
      @endforeach
    </select>
  </div>
</div>