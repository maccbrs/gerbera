
<div class="item form-group">
  <label class="control-label col-md-3" for="name">{{$label}}
  </label>
  <div class="col-md-9">
    <input class="form-control col-md-7 col-xs-12" name="{{$name}}" type="text" value="{{$value}}">
  </div>
</div>