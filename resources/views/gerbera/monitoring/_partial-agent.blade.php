          <table class="table table-bordered  table-hover">
            <thead>
              <tr>
                <th>Agent</th>
                <th>Campaign</th>
                <th>Login Time</th>
                <th>Current Status</th>
                <th>Break</th>
                <th>Lunch</th>
                <th>Meeting</th>
                <th>Other</th>
				@if(Auth::user()->user_type == 'admin')
				<th>Action</th>	
				@endif
              </tr>
            </thead>
            <tbody>
              @foreach($logs as $d)

               <?php  $exceed = explode(" ",$d['ago']); 

               $exceed_ind = null; ?>

               @if(!empty($exceed[1] ))

                  @if(($exceed[1] > 4) && ($exceed[2] == 'days'))
                    <?php $exceed_ind = "table-danger"; ?>
                  @endif

                  @if($exceed[2] == 'months')
                    <?php $exceed_ind = "table-danger"; ?>
                  @endif

              @endif 

              @if(!empty($d['user']['name']))
                <tr class="{{$exceed_ind}}">
                  <th scope="row">{{$d['user']['name']}}</th>
                  <td >{{$d['campaign']['name']}}</td>
                  <td >{{$d['login']}} ({{$d['ago']}})</td>
                  <td >{{$d['status']}}</td>
                  <td >{{$d['break']}}</td>
                  <td >{{$d['lunch']}}</td> 
                  <td >{{$d['meeting']}}</td>
                  <td >{{$d['other']}}</td> 
				  @if(Auth::user()->user_type == 'admin')
				  <!--<td > <a href="{{route('gerbera.forcetimeout',$d['id'])}}" target="_blank"><button> ForceLogOut</button></a>{{$d['id']}}</td>  -->
				  <td > <button onclick="loadDoc({{$d['id']}})"> ForceLogOut</button></td> 
				  @endif
                </tr> 
              @endif 
              
              @endforeach
            </tbody>
          </table>

        <div>Summary</div>
          <table class="table table-bordered">
            <thead>
              <tr>
                <th>Campaign</th>
                <th>Logged in</th>
                <th>Break</th>
                <th>Meeting</th>
                <th>Lunch</th>
                <th>Other</th>
                <th>total</th>
              </tr>
            </thead>
            <tbody>
              @foreach($summary as $k => $v)
              <tr>
                <th scope="row">{{$k}}</th>
                <td>{{(isset($v['logged in'])?$v['logged in']:'')}}</td>
                <td>{{(isset($v['break'])?$v['break']:'')}}</td>
                <td>{{(isset($v['meeting'])?$v['meeting']:'')}}</td>
                <td>{{(isset($v['lunch'])?$v['lunch']:'')}}</td>
                <td>{{(isset($v['other'])?$v['other']:'')}}</td>
                <td>{{(isset($v['total'])?$v['total']:'')}}</td>
              </tr>              
              @endforeach
            </tbody>
          </table>    

          <style>

            .table-danger{

              background-color: #f2dede;
              color:black;
            }

          </style>
		  
		  <script>
		      <?php $baseurl = URL::asset('/'); ?>
		  function loadDoc(id) {
				  var xhttp = new XMLHttpRequest();
				  xhttp.onreadystatechange = function() {
					if (this.readyState == 4 && this.status == 200) {
					 //document.getElementById("demo").innerHTML = this.responseText;
					 alert('Force Logout success');
					}
				  };
				  xhttp.open("GET", "{{$baseurl}}/forcetimeout/"  + id, true);
				  xhttp.send();
				}
		  
		  </script>