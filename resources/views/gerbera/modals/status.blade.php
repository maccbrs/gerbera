@if($status)

<button id="status" type="button" style="display:hidden" data-toggle="modal" data-target=".modalStatus"></button>
<div class="modal fade modalStatus" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title" id="myModalLabel">{{$status['msg']}}</h4>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">ok</button>
          </div>
        </div>
  </div>
</div>
<script type="text/javascript">
$('#status').trigger('click');
</script>

@endif
  