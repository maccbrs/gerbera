<button type="button" class="btn btn-primary fa fa-trash pull-right" data-toggle="modal" data-target=".modalDelete{{$id}}"></button>


  <div class="modal fade modalDelete{{$id}}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">

      <form class="form-horizontal form-label-left" method="post" action="{{route($route,$id)}}" novalidate>
        {{ csrf_field() }}
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
              </button>
              <h4 class="modal-title" id="myModalLabel">Are you sure you want to delete {{$label}}?</h4>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-primary">Proceed</button>
            </div>
          </div>
      </form>

    </div>
  </div>

