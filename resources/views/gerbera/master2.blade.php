<?php $asset = URL::asset('/'); ?> 
<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Real Time Application</title>

  <!-- Bootstrap core CSS -->

  <link href="{{$asset}}gentella/css/bootstrap.min.css" rel="stylesheet">
  <link href="{{$asset}}gentella/fonts/css/font-awesome.min.css" rel="stylesheet">
  <link href="{{$asset}}gentella/css/animate.min.css" rel="stylesheet">
  <link href="{{$asset}}gentella/css/custom.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="{{$asset}}gentella/css/maps/jquery-jvectormap-2.0.3.css" />
  <link href="{{$asset}}gentella/css/icheck/flat/green.css" rel="stylesheet" />
  <link href="{{$asset}}gentella/css/floatexamples.css" rel="stylesheet" type="text/css" />
  
  <link href="{{$asset}}datatable/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
  
  <script src="{{$asset}}gentella/js/jquery.min.js"></script>
  <script src="{{$asset}}gentella/js/nprogress.js"></script>
  <script src="{{$asset}}datatable/jquery.dataTables.min.js"></script>
  
  
  @yield('header-scripts')
    <style type="text/css">

      .rta_logo{

        width:75%;
      }

      .nav_title, .x_content{

        text-align: center;
      }

      .rta{

        color:white;
      }

      .loader_img{

        width:30%;
      }

      .nav_menu{

        background-color:transparent;

      }

      body.nav-sm .container.body .right_col {
          background: url(<?php echo $asset ?>gentella/images/background4.jpg) no-repeat center center fixed; 
          -webkit-background-size: cover;
          -moz-background-size: cover;
          -o-background-size: cover;
          background-size: cover; 
         
      } 

      .divbg{

        background-color: rgba(0,0,0,.6);
        color: white;
      }

      .btn-filter{

        background-color: transparent;

      }

      .column-title{

        text-align: center;

      }

      .timer , .timer2{

        margin-left: 20px;
        color: #60AEFF;
        font-size: 15px;
      }

      .ind{
        margin-left: 20px;
        color:#ffb160;
        font-size: 15px;
      }

     .time{
        margin-top: 16px;
      }



    </style> 
</head>


<body class="nav-sm">

  <div class="container body">


    <div class="main_container">

      <div class="col-md-3 left_col">
        <div class="left_col scroll-view"> 
        <br>
          <div class="navbar nav_title" style="border: 0;">

            <a href="{{route('gerbera.dashboard.index')}}" class="site_title"><img src = "{{$asset}}gentella/images/website_logo8.png" class = "rta_logo"> </a>
            <h2 class = "rta">RTA</h2>
          </div>
          <div class="clearfix"></div>

          <!-- menu prile quick info -->
          <div class="profile">
            <div class="profile_pic">
              <img src="{{$asset}}gentella/images/img.jpg" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
              <span>Welcome,</span>
              <h2>{{Auth::user()->name}}</h2>
            </div>
          </div>
          <!-- /menu prile quick info -->

          <br /><br>

          <!-- sidebar menu -->
          <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

            <div class="menu_section">
              <h3>General</h3>
              <ul class="nav side-menu">
                 <li>
                    <a href="{{route('timecheck')}}"><i class="fa fa-clock-o "></i>Timecheck<span class="fa fa-clock-o "></span></a>
                    
                </li> 

                @if(in_array(Auth::user()->user_type,['admin']))
                <li>
                    <a><i class="fa fa-users"></i>Users<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu" style="display: none">
                        <li>
                            <a href="{{route('gerbera.user.index')}}">index</a>
                        </li>                                      
                    </ul>
                </li> 
                <li>
                    <a><i class="fa fa-flag"></i>Campaigns<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu" style="display: none">
                        <li>
                            <a href="{{route('gerbera.campaigns.index')}}">index</a>
                        </li>                                      
                    </ul>
                </li>                 
                @endif

                @if(in_array(Auth::user()->user_type,['admin','operation']))
                <li class="active"><a><i class="fa fa-file-word-o"></i>Reports<span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">
                    <li><a href="{{route('gerbera.reports.generate')}}">Generate</a>
                    </li>   
                    <li><a href="{{route('gerbera.reports.account')}}">Per Account</a>
                    </li>  
                    <li><a href="{{route('gerbera.reports.agentlog')}}">Per Agent</a>
                    </li>                  
                  </ul>
                </li>
                @endif

              </ul>
            </div>

          </div>
          <!-- /sidebar menu -->


          <!-- /menu footer buttons -->
        </div>
      </div>

      <!-- top navigation -->
      <div class="top_nav">

        <div class="nav_menu">
          <nav class="" role="navigation">
           
            <div class="nav toggle">
              
            </div>
            <ul class="nav navbar-nav navbar-right">
          
              <li >
                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                  {{Auth::user()->name}}
                  <span class=" fa fa-angle-down"><label id="timer"></label>s</span>
                </a>
                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                  <li><a href="{{url('/logout')}}"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                  </li>
                </ul>
              </li>

              <li>
           <!--    <span class ="pull-left time">
              <label class ="ind">MNL: </label>
              <label class="timer" ></label>
              <label class ="ind"> | PST: </label>
              <label class="timer2" ></label>
            </span> -->
            </li>

            </ul>
          </nav>
        </div>

      </div>
      <!-- /top navigation -->

      @yield('content')


    </div>

  </div>

  <div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
  </div>

  <script src="{{$asset}}gentella/js/bootstrap.min.js"></script>

  <!-- gauge js -->
  <script type="text/javascript" src="{{$asset}}gentella/js/gauge/gauge.min.js"></script>
  <!-- bootstrap progress js -->
  <script src="{{$asset}}gentella/js/progressbar/bootstrap-progressbar.min.js"></script>
  <script src="{{$asset}}gentella/js/nicescroll/jquery.nicescroll.min.js"></script>
  <!-- icheck -->
  <script src="{{$asset}}gentella/js/icheck/icheck.min.js"></script>
  <!-- daterangepicker -->
  <script type="text/javascript" src="{{$asset}}gentella/js/moment/moment.min.js"></script>
  <script type="text/javascript" src="{{$asset}}gentella/js/datepicker/daterangepicker.js"></script>
  <!-- chart js -->
  <script src="{{$asset}}gentella/js/chartjs/chart.min.js"></script>

  <script src="{{$asset}}gentella/js/custom.js"></script>

  <!-- flot js -->
  <!--[if lte IE 8]><script type="text/javascript" src="js/excanvas.min.js"></script><![endif]-->
  <script type="text/javascript" src="{{$asset}}gentella/js/flot/jquery.flot.js"></script>
  <script type="text/javascript" src="{{$asset}}gentella/js/flot/jquery.flot.pie.js"></script>
  <script type="text/javascript" src="{{$asset}}gentella/js/flot/jquery.flot.orderBars.js"></script>
  <script type="text/javascript" src="{{$asset}}gentella/js/flot/jquery.flot.time.min.js"></script>
  <script type="text/javascript" src="{{$asset}}gentella/js/flot/date.js"></script>
  <script type="text/javascript" src="{{$asset}}gentella/js/flot/jquery.flot.spline.js"></script>
  <script type="text/javascript" src="{{$asset}}gentella/js/flot/jquery.flot.stack.js"></script>
  <script type="text/javascript" src="{{$asset}}gentella/js/flot/curvedLines.js"></script>
  <script type="text/javascript" src="{{$asset}}gentella/js/flot/jquery.flot.resize.js"></script>
  <!-- worldmap -->
  <script type="text/javascript" src="{{$asset}}gentella/js/maps/jquery-jvectormap-2.0.3.min.js"></script>
  <script type="text/javascript" src="{{$asset}}gentella/js/maps/gdp-data.js"></script>
  <script type="text/javascript" src="{{$asset}}gentella/js/maps/jquery-jvectormap-world-mill-en.js"></script>
  <script type="text/javascript" src="{{$asset}}gentella/js/maps/jquery-jvectormap-us-aea-en.js"></script>
  <script src="{{$asset}}gentella/js/pace/pace.min.js"></script>
  <script src="{{$asset}}gentella/js/skycons/skycons.min.js"></script>


  @yield('footer-scripts')

  <script type="text/javascript">


     function myTimer() {
            var d = new Date()
            if (d.getMinutes() == 0 ||d.getMinutes() == 30) {
              //console.log(d.getMinutes());
              location.reload();
            }
    }

    timer = setInterval(function(){myTimer()},60000);
  </script>
  <!-- /datepicker -->
  <!-- /footer content -->

  <!-- server clock -->
  <script> 
      setInterval(function() {
        var d = new Date();

          diem1 = "AM";
          var h1 = d.getHours();
          var m1 = d.getMinutes();
          var s1 = d.getSeconds();

          if (h1 == 0) {
              h1 = 12
          } else if (h1 > 12) {
              h1 = h1 - 12;
              diem1 = "PM";
          }

          var currentTime = new Date();
          currentTime.setHours(currentTime.getHours()+(-3));
          var diem = "AM";
          var h = currentTime.getHours();
          var m = currentTime.getMinutes();
          var s = currentTime.getSeconds();

          if (h == 0) {
              h = 12
          } else if (h > 12) {
              h = h - 12;
              diem = "PM";
          }

          if (h < 10) {
              h = "0" + h;
          }

          if (m < 10) {
              m = "0" + m;
          }

          if (s < 10) {
              s = "0" + s;
          }

          mnl = h1 + ":" + m1 + ":" + s1 + " " + diem1;
          pst = h + ":" + m + ":" + s + " " + diem;
          $('.timer').text(mnl);
          $('.timer2').text(pst);
  
      }, 1000);
  </script> 

</body>

</html>
