@extends('gerbera.master')

@section('content')
<div class="container">
    <div class="row">
         <div class="panel panel-default">
            <div class="panel-heading">
            <div><h3><span class = "time_label"> PST Time : </span><span class = "time"> {{$pst_time}} </span></h3></div>
            <div><h3><span class = "time_label"> Manila Time : </span><span class = "time">  {{$manila_time}} </span></h3></div></div>
        </div>
    </div>
</div>

<script>
    
     setInterval(function() {
      window.location.reload();
    }, 10000); 

</script>

@endsection
