<?php

namespace App\Http\Middleware;
use Auth;
use Closure;

class checkTimekeeper
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $gn = new \App\Http\Controllers\gerbera\generalController;
        $auth = Auth::user();
        if($auth->user_type == 'agent' && $gn->uriprefix($request->route()->getUri()) != 'timekeeper'):
            return redirect()->route('gerbera.timekeeper.index');
        endif;
        return $next($request);
    }
}
 