<?php namespace App\Http\Middleware;

use Closure;
use Auth;

class additionalAuthInfo
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            }

            return redirect()->guest('login');
        }

        $gn = new \App\Http\Controllers\gerbera\generalController;
        $auth = Auth::user();
        if($auth->user_type == 'agent' && $gn->uriprefix($request->route()->getUri()) != 'timekeeper'):
            return redirect()->route('gerbera.timekeeper.index');
        endif;

        if($auth->user_type != 'agent' && $gn->uriprefix($request->route()->getUri()) == 'timekeeper'):
            return redirect()->route('gerbera.dashboard.index');
        endif;
        
        return $next($request);
    }
}
 