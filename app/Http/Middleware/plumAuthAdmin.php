<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class plumAuthAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(Auth::user()->user_type != 'admin'):
            return redirect()->back();
        endif;
        return $next($request); 


    }
}
