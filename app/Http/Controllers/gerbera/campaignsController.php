<?php namespace App\Http\Controllers\gerbera;

use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

class campaignsController extends Controller
{

	public function index(){

		$Campaigns = new \App\Http\Models\gerbera\Campaigns;
		$items = $Campaigns->get();
		return view('gerbera.campaigns.index',compact('items'));
	}

	public function create(Request $r){

		$Gn = new \App\Http\Controllers\gerbera\generalController;
		$Campaigns = new \App\Http\Models\gerbera\Campaigns;
		$Campaigns->create($r->all());
		return redirect()->back();
	}

	public function destroy($id){

		$Gn = new \App\Http\Controllers\gerbera\generalController;
		$Campaigns = new \App\Http\Models\gerbera\Campaigns; 
		$Campaigns->find($id)->delete();
		return redirect()->back();	

	}

	public function switchCampaign(Request $r){

		$Log = new \App\Http\Models\gerbera\Log; 
		$log = $Log->find($r->id);
		$log->update(['campaign_id' => $r->campaign_id]);
		return redirect()->back();

	}

}