<?php namespace App\Http\Controllers\gerbera;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;

class dataController extends Controller
{


	public function update(Request $r,$id){

		$gn = new \App\Http\Controllers\gerbera\generalController;

		$this->save($r,$id,$r->issave);


		$this->session_update((in_array($r->next_action,['break','lunch','meeting','logout','pullnext'])?$r->next_action:'other'));


		

		if($r->next_action != 'logout'):
			if(in_array($r->issave,['skipped','done','incomplete'])): 
				if($r->next_action == 'pullnext'):
					return redirect()->back();
				endif;
			endif; 
			return redirect()->route('gerbera.timeout.pause');					
		endif;

		return redirect()->back();

	}

	public function save($r,$id,$status = 'done'){

		$now = Carbon::now()->format('Y-m-d H:i:s');
		$gn = new \App\Http\Controllers\gerbera\generalController;
		$MetaData = new \App\Http\Models\gerbera\MetaData;
		$Data = new \App\Http\Models\gerbera\Data;
		$logObj = new \App\Http\Models\gerbera\Log;
		$record = $logObj->active(Auth::user()->id)->first();

		$metadata = $MetaData->find($id);
		$Data->find($metadata->data_id)->update(['status' => $status,'metadata_id' => $id]);

		$metadata->content = json_encode($r->except('_token','next_action','issave'));
		if($status == 'pause'):
			$metadata->pause_time = $now;
		else:
			$metadata->end = $now;
			$metadata->log_id = $record->id;
		endif;
		
		$metadata->status = $status;
		$metadata->save();

	} 

	public function session_update($status){

		$gn = new \App\Http\Controllers\gerbera\generalController;
		if($status == 'pullnext') return true;
		
		$now = Carbon::now()->format('Y-m-d H:i:s');
		$Log = new \App\Http\Models\gerbera\Log;
		$LogActions = new \App\Http\Models\gerbera\LogActions;

		$log = $Log->active(Auth::user()->id)->first();
		$log->status = $status;
		$log->updated_at = $now;
		$log->last_updated = $now;

		if($status == 'logout'):
			$log->logout = $now;
			$log->status = 'logged out';
		endif;
		$log->save();

		$log_actions = $LogActions->where('log_id',$log->id)->first();
		$logtxt = $now.'|| '.$log->status;
		if($log_actions):
			$log_actions->content = $gn->jasonizer($log_actions->content,$logtxt);
			$log_actions->updated_at = $now;
			$log_actions->save();
		endif;			


	}

}