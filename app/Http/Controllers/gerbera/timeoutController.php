<?php namespace App\Http\Controllers\gerbera;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;

class timeoutController extends Controller
{

	public function pause(){


		$gn = new \App\Http\Controllers\gerbera\generalController;
		$LogActions = new \App\Http\Models\gerbera\LogActions;

		$time = Carbon::now()->format('Y-m-d H:i:s');
		$Log = new \App\Http\Models\gerbera\Log;
		$log = $Log->active(Auth::user()->id)->first();


		if($log):

			$log_actions = $LogActions->where('log_id',$log->id)->first();
			if(!$log_actions):
				$logtxt = $time.'|| '.$log->status;
				$log_actions = $LogActions->create([
					'log_id' => $log->id,
					'content' => $gn->jasonizer('',$logtxt),
					'created_at' => $time,
					'updated_at' => $time
				]);
			endif;

			$updated_log = $gn->update_logs($log_actions->content);
			$log->update($updated_log);

			if(!$log || $log->status == 'logged in'):
				return redirect()->route('gerbera.dashboard.index');
			endif;	
			
			foreach (['meeting','break','lunch','other'] as $v):
				if(isset($updated_log[$v])):
					$session[$v] = $updated_log[$v];
				else:
					$session[$v] = gmdate("H:i:s",0);
				endif;
			endforeach;
			//$gn->pre($session);die;
			$log = $log->toArray();

			return view('gerbera.timeout.pause',compact('sec_passed','session','log','gn','time'));

		else:
			return view('gerbera.timeout.loggedout',compact('gn'));
		endif;


		

	}


	public function back_to_work(){

		$carbon = new \Carbon\Carbon;
		$now = $carbon->now()->format('Y-m-d H:i:s');
		$gn = new \App\Http\Controllers\gerbera\generalController;
		$Log = new \App\Http\Models\gerbera\Log;
		$LogActions = new \App\Http\Models\gerbera\LogActions;
		$log = $Log->active(Auth::user()->id)->with(['log_actions'])->first();
		$log_actions = $LogActions->where('log_id',$log->id)->first();

		$logtxt = $now.'|| logged in';
		if($log_actions):
			$log_actions->content = $gn->jasonizer($log_actions->content,$logtxt);
			$log_actions->updated_at = $now;
			$log_actions->save();
		endif;	

		$log_arr = $log->toArray();	
		
		if($log && $log_arr['status'] != 'logged in'):

			$updated_log = $gn->update_logs($log_actions->content);
			$updated_log['status'] = 'logged in';
			$log->update($updated_log);

		endif;	

		return redirect()->route('gerbera.dashboard.index'); 

	}


}