<?php namespace App\Http\Controllers\gerbera;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;

class monitoringController extends Controller
{

	public function agent(){ 

		$Log = new \App\Http\Models\gerbera\Log;
		$gn = new \App\Http\Controllers\gerbera\generalController;
		$logs = $Log->where('status','!=','logged out')->with(['user','campaign'])->orderBy('campaign_id','desc')->get()->toArray();
		$summary = $this->summary($logs);

		return view('gerbera.monitoring._partial-agent',compact('logs','summary'));
	} 

	public function filter(Request $r){ 
		$campaign = $r['campaign']; 
		$c = explode(",", $campaign);

		foreach ($c as $key => $campaign_list) {
			
			
			$campaign_array[] = $campaign_list;
			
		}

		$Log = new \App\Http\Models\gerbera\Log;
		$gn = new \App\Http\Controllers\gerbera\generalController;
		$logs = $Log->where('status','!=','logged out')->with(['user','campaign'])->wherein('campaign_id',$campaign_array)->orderBy('campaign_id','desc')->get()->toArray();
		$summary = $this->summary($logs);

		return view('gerbera.monitoring._partial-agent',compact('logs','summary'));
	} 

	public function test(){ 

		$Log = new \App\Http\Models\gerbera\Log;
		$gn = new \App\Http\Controllers\gerbera\generalController;
		$logs = $Log->where('status','!=','logged out')->with(['user','campaign'])->orderBy('campaign_id','desc')->get()->toArray();
		$summary = $this->summary2($logs);
		$gn->pre($summary);

		
	}

	public function summary($items){

		$accounts = [];
		foreach ($items as $v):
			if(array_key_exists($v['campaign']['campaign_id'], $accounts)):
				$accounts[$v['campaign']['campaign_id']]['total']++;
			else:
				$accounts[$v['campaign']['campaign_id']]['total'] = 1;
			endif;
		endforeach;

		foreach ($items as $v):
			if(isset($accounts[$v['campaign']['campaign_id']][$v['status']])):
				$accounts[$v['campaign']['campaign_id']][$v['status']]++;
			else:
				$accounts[$v['campaign']['campaign_id']][$v['status']] = 1;
			endif;
		endforeach;

		return $accounts;

	}

	public function summary2($items){

		$accounts = [];
		foreach ($items as $v):
			if(array_key_exists($v['campaign']['campaign_id'], $accounts)):
				$accounts[$v['campaign']['campaign_id']]['total']++;
			else:
				$accounts[$v['campaign']['campaign_id']]['total'] = 1;
			endif;
		endforeach;

		foreach ($items as $v):
			if(isset($accounts[$v['campaign']['campaign_id']][$v['status']])):
				$accounts[$v['campaign']['campaign_id']][$v['status']]++;
			else:
				$accounts[$v['campaign']['campaign_id']][$v['status']] = 1;
			endif;
		endforeach;

		return $accounts;

	}

}