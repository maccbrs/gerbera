<?php namespace App\Http\Controllers\gerbera;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Excel;
use Auth;

class reportsController extends Controller
{

	public function generate(){

		$Campaigns = new \App\Http\Models\gerbera\Campaigns;

		$campaigns = $Campaigns->select('id','name')->get();
		
		$report_type = 'daily';

		return view('gerbera.reports.daily',compact('campaigns','report_type'));
	}

	public function account(){

		$Campaigns = new \App\Http\Models\gerbera\Campaigns;
		$campaigns = $Campaigns->select('id','name')->get();

		return view('gerbera.reports.account',compact('campaigns','test'));

	}

	public function generate_post(Request $r){

		$gn = new \App\Http\Controllers\gerbera\generalController;
		$Campaigns = new \App\Http\Models\gerbera\Campaigns;
		$campaigns = $Campaigns->select('id','name')->get();

		switch ($r->reporttype) {
			case 'agent':
				$rows = $this->agent($r->from,$r->to);
				return view('gerbera.reports.agent',compact('rows','gn'));
				break;
			case 'downloadable':
				$rows = $this->downloadable($r->from,$r->to);
				break;
			case 'agent-time':
			
				$rows = [];
				$rows = $this->agent_time($r->from,$r->to,$r->campaign); 
				$carbon = new Carbon;
				return view('gerbera.reports.agent-time',compact('rows','carbon','gn','campaigns'));
				break;				
		}

	}

	public function agent_time($fr,$to,$campaign){

		$gn = new \App\Http\Controllers\gerbera\generalController;
		$Log = new \App\Http\Models\gerbera\Log;
		return $Log->whereBetween('login', array($fr, $to))
			->where('status','logged out')
			->whereIn('campaign_id', $campaign)
			->orderBy('user_id','desc')
			->orderBy('login','desc')
			->with(['user'])
			->with(['campaign'])
			->get();

	}

	public function agent($fr,$to){

		$gn = new \App\Http\Controllers\gerbera\generalController;
		$Data = new \App\Http\Models\gerbera\Data;
		$data = $Data->whereIn('status',['done','skipped'])->whereBetween('updated_at', array($fr, $to))->with(['metadata'])->get();
		$users = [];		

		foreach($data as $d):
			$users[$d->metadata['user']['name']][] = [
				'status' => $d->metadata['status'],
				'content' => $d->metadata['content'],
				'start' => $d->metadata['start'],
				'end' => $d->metadata['end'],
				'pause' => $d->metadata['pause']
			]; 
		endforeach;

		$rows = [];
		foreach ($users as $k => $data):
			foreach ($data as $v):
				if(!isset($rows[$k]['time_spent'])) $rows[$k]['time_spent'] = 0;
				$rows[$k][$v['status']] = (isset($rows[$k][$v['status']])?$rows[$k][$v['status']] + 1:1);
				$rows[$k]['time_spent'] = $rows[$k]['time_spent'] + $gn->sec_between_2_time($v['start'],$v['end']) - $v['pause'];
			endforeach;
		endforeach;

		return $rows;
	
	}

	public function generate_account(Request $r){

		$campaigns = $r['campaign'];

		$from = $r['from'];
		$from = str_replace('/', '-', $from);
		$from = date("Y-m-d H:i:s", strtotime($from)); 

		$to = $r['to'];
		$to = str_replace('/', '-', $to);
		$to = date("Y-m-d H:i:s", strtotime($to)); 
		
		$Log = new \App\Http\Models\gerbera\Log;

		$loglist = $Log->whereBetween('login', [$from, $to])->wherein('campaign_id',$campaigns )->orderBy('login','asc')->with(['user'])->with(['campaign'])->get();
		
		$sheetname = 'RTA Report (' . date("Y-m-d", strtotime($from)) . ' - ' . date("Y-m-d", strtotime($to)) .')'; 

		Excel::create($sheetname, function($excel ) use($loglist) {

			foreach ($loglist as $key => $value) {
				
				$agentname = $value['user']['name'];
				$account = $value['campaign']['name'];
				$peraccount[$account][$agentname]['login'][$key] =  date("m-d-Y", strtotime($value['login'])); 
				$peraccount[$account][$agentname]['account'] =  $account; 

				$startcalendar = date("Y-m-d", strtotime($loglist[0]['login'])); 
			}

			$endcalendar= date("Y-m-d", strtotime($value['login'])); 

			$output_format = 'm-d-Y';
			$step = '+1 day';
			
			$dates = array();
		    $current = strtotime($startcalendar);
		    $last = strtotime($endcalendar);


		    while( $current <= $last ) {

		        $dates[] = date($output_format, $current);
		        $current = strtotime($step, $current);

		    }

		    $peragent = array();

		    $ctr = 0;
	    	foreach ($peraccount as $perkey => $accountlist) {
	    		
	    		foreach ($accountlist as $agentkey => $agentlist) {
	    			
	    			foreach ($agentlist as $loginkey => $loginlist) {
	    				foreach ($dates as $datekey => $date) {

	    					if(in_array($date, $agentlist['login'])){

	    						$peragent[$agentkey][$date][$ctr] = $perkey; 
		    					$rtadata[$perkey][$agentkey]['login'][$date] = 'X'; 
		    					$rtadata[$perkey][$agentkey]['account'][$date] = $perkey; 

	    					}else{

	    						$peragent[$agentkey][$date][$ctr] = '';
	    						$rtadata[$perkey][$agentkey]['login'][$date] = ''; 
	    						$rtadata[$perkey][$agentkey]['account'][$date] = ''; 

	    					}
						}	
		    		}
		    		$ctr ++;
		    	}
		    }


		ksort($peragent); 

			$excel->sheet('Sheetname', function($sheet) use($dates,$rtadata,$peragent){

				$loadCount = 0;
				$test = 8;
				$dates = $dates;

				$sheet->loadView('gerbera.reports.excel')->with('dates', $dates)->with('peraccount', $rtadata)->with('peragent', $peragent);
				
			});

		})->download('xlsx');
	
	}

	public function downloadable($fr,$to){

		$gn = new \App\Http\Controllers\gerbera\generalController;
		$Data = new \App\Http\Models\gerbera\Data;		
		$dat = $Data->whereIn('status',['done'])->whereBetween('updated_at', array($fr, $to))->with(['metadata'])->get();
		
		$rows = [];

		foreach ($dat as $k => $v):
			$rows[$k] = [
				'agent' => $v->metadata['user']['name'],
				'start_time' => $v->metadata['start'],
				'end_time' => $v->metadata['end'],
				'pause' => $v->metadata['pause']
			];
			$content = json_decode($v->metadata['content'],true);
			$rows[$k] = array_merge($rows[$k],$content);
		endforeach;
		//$gn->pre($rows);
    	$keys = [];
    	$data = [];

    	foreach ($rows as $a):
    		foreach ($a as $ak => $av):
    			if(!in_array($ak,$keys)):
    				if(!is_array($av)):
    					$keys[] = $ak;
    				endif;
    			endif;
    		endforeach;
    	endforeach;

    	foreach ($rows as $ctr => $a):
    		foreach ($keys as $key):
    			$data[$ctr][$key] = (isset($a[$key])?$a[$key]:'');
    		endforeach;
    	endforeach;

    	$formatted_keys = [];
    	foreach ($keys as $k):
    		$formatted_keys[] = $gn->format_title($k);
    	endforeach;


		Excel::create('data', function($excel) use ($formatted_keys,$data) {
		    $excel->sheet('Sheet1', function($sheet) use ($formatted_keys,$data) {
					$sheet->row(1,$formatted_keys);
					$ctr = 2;
					foreach ($data as $k => $v):
							$sheet->row($ctr,$v);
							$ctr++;						
					endforeach;
		    });
		})->export('xls');

	}

	public function agentrecord(){

		$gn = new \App\Http\Controllers\gerbera\generalController;
		$Log = new \App\Http\Models\gerbera\Log;
		$logs = $Log->where('user_id',Auth::user()->id)->where('status','logged out')->orderBy('login','desc')->take(30)->get();
		$rows = [];
		foreach ($logs as $k => $v):
			$rows[] = [
				'login' => $v->login,
				'break' => $v->break,
				'lunch' => $v->lunch,
				'meeting' => $v->meeting,
				'other' => $v->other,
				'logged out' => $gn->haveLogOut($v),
				'hours' => $gn->getHoursDuty($v)
			];
		endforeach;

		return view('gerbera.reports.agentrecord',compact('rows'));

	}

	public function agent_log(){
	
		$Campaigns = new \App\Http\Models\gerbera\Campaigns;
		$User = new \App\Http\Models\gerbera\User;

		$campaigns = $Campaigns->select('id','name')->get();
		$users = $User->select('id','name')->orderBy('name')->get();

		/*print_r('<pre>'); print_r($users); print_r('</pre>'); exit; 
*/
		$report_type = 'agentlog';

		return view('gerbera.reports.daily',compact('campaigns','report_type','users'));

	}

	public function generate_agent(Request $r){

		$gn = new \App\Http\Controllers\gerbera\generalController;
		$Campaigns = new \App\Http\Models\gerbera\Campaigns;
		$User = new \App\Http\Models\gerbera\User;
		$users = $User->select('id','name')->orderBy('name')->get();
		$campaigns = $Campaigns->select('id','name')->get();

		$rows = [];
		$rows = $this->agent_logs_action($r->from,$r->to,$r->agent); 

		$logactivities = [];

		if(!empty($rows)){

			foreach ($rows as $key => $value) {

				if(empty($value->log_actions)){
				//dd($value->id);
				}
				else{
				$logactivities[$key]['name'] = $value->user['name'];
				$logactivities[$key]['login'] = date("F j, Y", strtotime($value['login'])); 
				
				$content = json_decode($value->log_actions->content);
				foreach ($content as $key2 => $value2) {
					$logs = explode("||", $value2);
					$logactivities[$key]['content'][$key2]['sessionname'] = $logs[1];
					$logactivities[$key]['content'][$key2]['time'] = $logs[0];
					
				}
				}
			}
		}

		$carbon = new Carbon;
		return view('gerbera.reports.agent-log-post',compact('rows','carbon','gn','campaigns','logactivities','users'));

	}

	public function agent_logs_action($fr,$to,$agent){
		
		$gn = new \App\Http\Controllers\gerbera\generalController;
		$Log = new \App\Http\Models\gerbera\Log;

		return $Log->whereBetween('login', array($fr, $to))
			->whereIn('user_id', [$agent])
			->orderBy('login','desc')
			->with(['user'])
			->with(['log_actions'])
			->get();

	}


}