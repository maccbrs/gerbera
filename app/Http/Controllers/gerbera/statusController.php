<?php namespace App\Http\Controllers\gerbera;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;

class statusController extends Controller
{



	public function timein(Request $r){

		$this->validate($r, [
		    'campaign_id' => 'required'
		]);


		$logObj = new \App\Http\Models\gerbera\Log;
		$accLogObj = new \App\Http\Models\gerbera\AccountLog;
		$gn = new \App\Http\Controllers\gerbera\generalController;

		$loggedin = $logObj->active(Auth::user()->id)->first();	

		if(!$loggedin):

			$now = Carbon::now();
			$data = [
				'user_id' => Auth::user()->id,
				'account_id' => $r->input('account_id'),
				'status' => 'logged in',
				'campaign_id' => $r->campaign_id,
				'login' => $now->format('Y-m-d H:i:s'),
				'break' => gmdate("H:i:s", 0),
				'meeting' => gmdate("H:i:s", 0),
				'lunch' => gmdate("H:i:s", 0),
				'other' => gmdate("H:i:s", 0)
			];

			$loggedin = $logObj->create($data);
			$loggedin2 = $logObj->active(Auth::user()->id)->first();

			$accLogObj->create([
				'log_id' => $loggedin2->id,
				'login' => $now->format('Y-m-d H:i:s'),
				'created_at' => $now->format('Y-m-d H:i:s'),
				'updated_at' => $now->format('Y-m-d H:i:s'),
				'account_id' => $r->input('account_id')
			]); 

		endif;	

		return redirect()->back();

	}

	public function timeout(){

		$logObj = new \App\Http\Models\gerbera\Log;
		$accLogObj = new \App\Http\Models\gerbera\LogActions;
		$gn = new \App\Http\Controllers\gerbera\generalController;
		$record = $logObj->active(Auth::user()->id)->first();	
		if($record):
			$record->update(['status' => 'logged out','logout' => Carbon::now()->format('Y-m-d H:i:s')]);
			$accntlogObj = new \App\Http\Models\gerbera\AccountLog;
			$accntlogObj->where('log_id',$record->id)->where('account_id',$record->account_id)->update(['logout' => Carbon::now()->format('Y-m-d H:i:s')]);
		endif;	

		return redirect()->back();		
	}

	public function update(Request $r){

		$logObj = new \App\Http\Models\gerbera\Log;
		$accLogObj = new \App\Http\Models\gerbera\LogActions;
		$gn = new \App\Http\Controllers\gerbera\generalController;
		$dispo_list = [];

		$record = $logObj->active(Auth::user()->id)->first();
		$date = Carbon::now()->format('Y-m-d H:i:s');
		$validator = $accLogObj->where('log_id',$r->input('log_id'))->first();

		$array_sessions = json_decode($validator['content']);

		$indicator = 0;

		if(count($array_sessions) != 0){
			
			foreach ($array_sessions as $key => $value) {
				
				$get_dispo = explode("||", $value);
				$dispo_list[] = $get_dispo[1];

			}

		} 	

		if($dispo_list[count($dispo_list) - 1] != " logged in"){

			if($dispo_list[count($dispo_list) - 2] != " logged in"){

				$indicator = 1;

			}else{

				$indicator = 0;
			}

		}else{

			$indicator = 1;
		}

		if($indicator == 1){

			$record->update([

				'status' => $r->input('status'),
				'break' => gmdate("H:i:s", $r->input('break')),
				'meeting' => gmdate("H:i:s", $r->input('meeting')),
				'lunch' => gmdate("H:i:s", $r->input('lunch')),
				'other' => gmdate("H:i:s", $r->input('other')),
				'last_updated' => $date]
			);

			$acc_log = $accLogObj->where('log_id',$record->id)->first();

			$logtxt = $date.'||'.$r->input('status');

			if($acc_log):
				$acc_log->content = $gn->jasonizer($acc_log->content,$logtxt);
				$acc_log->updated_at = $date;
				$acc_log->save();
				echo 1;die;
			else:
				$accLogObj->create([
					'log_id' => $record->id,
					'content' => $gn->jasonizer('',$logtxt),
					'created_at' => $date,
					'updated_at' => $date
				]);
			endif;
		}

	}


	public function change(Request $r){

		$logObj = new \App\Http\Models\gerbera\Log;
		$accLogObj = new \App\Http\Models\gerbera\LogActions;
		$gn = new \App\Http\Controllers\gerbera\generalController;

		$record = $logObj->active(Auth::user()->id)->first();
		$date = Carbon::now()->format('Y-m-d H:i:s');
		$record->status = ($r->status == 'logout'?'logged out':$r->status);
		if($r->status == 'logout'):
			$record->logout = $date;
			$record->status = 'logged out';
			$record->last_updated = $date;
			$record->save();
			return redirect()->route('gerbera.dashboard.index');			
		else:
			$record->status = $r->status;
			$record->last_updated = $date;
			$record->save();
			return redirect()->route('gerbera.timeout.pause');
		endif;

	}



}