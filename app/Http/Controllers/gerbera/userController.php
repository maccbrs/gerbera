<?php namespace App\Http\Controllers\gerbera;

use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

class userController extends Controller
{

	public function index(){
		$usersObj = new \App\Http\Models\gerbera\User;
		$gn = new \App\Http\Controllers\gerbera\generalController;
		$users = $usersObj->where('tag_deleted',0)->with(['settings'])->get()->sortBy('name');
		//$gn->pre($users->toArray());
		return view('gerbera.user.index',compact('users','gn'));
	}

	public function destroy($id){
		$usersObj = new \App\Http\Models\gerbera\User;
		$user = $usersObj->where('id',$id)->with('settings')->first();
		$user->tag_deleted = 1; 
		$user->save();
		//if($user->settings)$user->settings->delete();
		//$user->delete();
		return redirect()->back();
	}
	public function reset_pass($id){
		$usersObj = new \App\Http\Models\gerbera\User;
		$user = $usersObj->where('id',$id)->with('settings')->first();
		$user->password = bcrypt('Magellan01!');
		$user->save();
		
		$usersObjPass = new \App\Http\Models\gerbera\UserSettings;
		$userpass_search = $usersObjPass->where('user_id',$id)->first();
		if(!empty($userpass_search))
		{
			$userpass = $usersObjPass->where('user_id',$id)->first();
			$userpass->password = 'Magellan01!';
			$userpass->save();
		}
		else{
			$userpass =  new \App\Http\Models\gerbera\UserSettings;
			$userpass->user_id = $id;
			$userpass->user_type = $user->user_type;
			$userpass->password = 'Magellan01!';
			$userpass->save();
		}

		
		//if($user->settings)$user->settings->delete();
		//$user->delete();
		return redirect()->back();
	}

	public function update(Request $r,$id){

		$gn = new \App\Http\Controllers\gerbera\generalController;
		$usersObj = new \App\Http\Models\gerbera\User;
		$user =	$usersObj->find($id);
		$input = $r->all();
		//$x = $gn->userOptions_putter($user->options,'campaign_id',$r->input('campaign_id')); $gn->pre($x);
		$input['options'] = $gn->userOptions_putter($user->options,'campaign_id',$r->input('campaign_id'));
		$user->update($input);
		return redirect()->back();

	}

	public function create(Request $r){

		$gn = new \App\Http\Controllers\gerbera\generalController;
		$usersObj = new \App\Http\Models\gerbera\User;
		$input = $r->all();
		$input['password'] = bcrypt($r->input('password'));
		$options['campaign_id'] = $r->input('campaign_id');
		$input['options'] = json_encode($options);
		$user = $usersObj->create($input);
		$user->settings()->create(['user_type' => $r->input('user_type'),'password' => $r->input('password')]);
		return redirect()->back();

	}

}