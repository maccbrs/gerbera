<?php namespace App\Http\Controllers\gerbera;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;
use Session;

class timekeeperController extends Controller
{


	public function index(){

		$ip = request()->ip();
		$gn = new \App\Http\Controllers\gerbera\generalController;
		$time = Carbon::now()->format('Y-m-d H:i:s');
		$Campaigns = new \App\Http\Models\gerbera\Campaigns;
		$campaigns = $Campaigns->get();	

		$Log = new \App\Http\Models\gerbera\Log;
		$LogActions = new \App\Http\Models\gerbera\LogActions;	
		$log = $Log->active(Auth::user()->id)->with(['log_actions'])->first();
		$logs = $Log->where('user_id',Auth::user()->id)->orderBy('created_at','desc')->paginate(20);
		if($log):
			if($log->log_actions):

				$log_actions = $log->log_actions;
				$a = json_decode($log->log_actions->content);
				$log->status = $this->func_status(end($a));

			else:

				$logtxt = $time.'||'.$log->status;
				$log_actions = $LogActions->create([
					'log_id' => $log->id,
					'content' => $gn->jasonizer('',$logtxt)
				]);	

			endif;

				$updated_log = $gn->update_logs($log_actions->content);
				$log->update($updated_log);	

				foreach (['meeting','break','lunch','other'] as $v):
					if(isset($updated_log[$v])):
						$session[$v] = $updated_log[$v]; 
					else:
						$session[$v] = gmdate("H:i:s",0);
					endif;
				endforeach;	

			return view('gerbera.timekeeper.index',compact('campaigns','log','session','gn','ip','logs'));
		else:
			return view('gerbera.timekeeper.out',compact('campaigns'));
		endif;

	}

	public function run(Request $r){

		$gn = new \App\Http\Controllers\gerbera\generalController;
		$time = Carbon::now()->format('Y-m-d H:i:s');
		$Log = new \App\Http\Models\gerbera\Log;
		$LogActions = new \App\Http\Models\gerbera\LogActions;	
		$LogFootprints = new \App\Http\Models\gerbera\LogFootprints;
		$log = $Log->active(Auth::user()->id)->with(['log_actions','log_footprints'])->first();	
		//$gn->pre($log);
		if($log):	

			$fprintstext = $time.'||'.$r->ip.'||'.$r->createdkey.'||'.$r->enteredkey.'||'.$r->status;
			if($log->log_footprints):
				$log->log_footprints->content = $gn->jasonizer($log->log_footprints->content,$fprintstext);
				$log->log_footprints->save();
			else:
				$LogFootprints->create([
					'log_id' => $log->id,
					'content' => $gn->jasonizer('',$fprintstext),
					'created_at' => $time,
					'updated_at' => $time
				]);				
			endif;

			if($r->enteredkey != $r->createdkey):
				return redirect()->back()->with('msg', 'The key you entered didn\'t match with the given');
			endif;

			$logtxt = $time.'||'.$r->status;

			if($log->log_actions):
				$log->log_actions->content = $gn->jasonizer($log->log_actions->content,$logtxt);
				$log->log_actions->updated_at = $time;
				$log->log_actions->save();
				$log_actions = $log->log_actions;
			else:
				$log_actions = $LogActions->create([
					'log_id' => $log->id,
					'content' => $gn->jasonizer('',$logtxt),
					'created_at' => $time,
					'updated_at' => $time
				]);
			endif;

			$updated_log = $gn->update_logs($log_actions->content);
			$log->update($updated_log);			
			
		endif;
		
		return redirect()->back();
	}

	public function switchs(Request $r){

		$Log = new \App\Http\Models\gerbera\Log; 
		$log = $Log->find($r->id);
		$log->update(['campaign_id' => $r->campaign_id]);
		return redirect()->back();

	}

	public function timeout(){

		$logObj = new \App\Http\Models\gerbera\Log;
		$accLogObj = new \App\Http\Models\gerbera\LogActions;
		$gn = new \App\Http\Controllers\gerbera\generalController;
		$record = $logObj->active(Auth::user()->id)->first();	
		if($record):
			$record->update(['status' => 'logged out','logout' => Carbon::now()->format('Y-m-d H:i:s')]);
			$accntlogObj = new \App\Http\Models\gerbera\AccountLog;
			$accntlogObj->where('log_id',$record->id)->where('account_id',$record->account_id)->update(['logout' => Carbon::now()->format('Y-m-d H:i:s')]);
		endif;	

		return redirect()->back();		
	}
	public function forcelogout($logid){
		//dd($logid);
		
		$logObj = new \App\Http\Models\gerbera\Log;
		$accLogObj = new \App\Http\Models\gerbera\LogActions;
		$gn = new \App\Http\Controllers\gerbera\generalController;
		$record = $logObj->where('id',$logid)->first();	
		if($record):
			$record->update(['status' => 'logged out','logout' => Carbon::now()->format('Y-m-d H:i:s')]);
			$accntlogObj = new \App\Http\Models\gerbera\AccountLog;
			$accntlogObj->where('log_id',$record->id)->where('account_id',$record->account_id)->update(['logout' => Carbon::now()->format('Y-m-d H:i:s')]);
		endif;	
		echo 'test' . $logid;
		//return redirect()->back();		
	}

	public function timein(Request $r){

		$this->validate($r, [
		    'campaign_id' => 'required'
		]);


		$logObj = new \App\Http\Models\gerbera\Log;
		$accLogObj = new \App\Http\Models\gerbera\AccountLog;
		$gn = new \App\Http\Controllers\gerbera\generalController;

		$loggedin = $logObj->active(Auth::user()->id)->first();	

		if(!$loggedin):

			$now = Carbon::now();
			$data = [
				'user_id' => Auth::user()->id,
				'account_id' => $r->input('account_id'),
				'status' => 'logged in',
				'campaign_id' => $r->campaign_id,
				'login' => $now->format('Y-m-d H:i:s'),
				'break' => gmdate("H:i:s", 0),
				'meeting' => gmdate("H:i:s", 0),
				'lunch' => gmdate("H:i:s", 0),
				'other' => gmdate("H:i:s", 0)
			];

			$loggedin = $logObj->create($data);
			$loggedin2 = $logObj->active(Auth::user()->id)->first();

			$accLogObj->create([
				'log_id' => $loggedin2->id,
				'login' => $now->format('Y-m-d H:i:s'),
				'created_at' => $now->format('Y-m-d H:i:s'),
				'updated_at' => $now->format('Y-m-d H:i:s'),
				'account_id' => $r->input('account_id')
			]); 

		endif;	

		return redirect()->back();

	}


	private function func_status($a){
		$b = explode('||', $a);
		return trim($b[1]);
	}
}