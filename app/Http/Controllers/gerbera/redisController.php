<?php namespace App\Http\Controllers\gerbera;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;
use Illuminate\Support\Facades\Redis;

class redisController extends Controller
{

	public function index(){

		$rd = app()->make('redis');
		$rd->set('key1','k1value');
		return $rd->get('key1');
	}

}