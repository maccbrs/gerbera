<?php namespace App\Http\Controllers\gerbera;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;

class testController extends Controller
{


	public function index(){
		$Log = new \App\Http\Models\gerbera\Log;
		$Gn = new \App\Http\Controllers\gerbera\generalController;
		$logs = $Log->where('status', '!=', 'logged out')->with(['log_actions'])->get();

		foreach ($logs as $v) {
			$updated_log = $Gn->update_logs($v->log_actions['content']);
			$Gn->pre($updated_log,false);
			$v->update($updated_log);
		}

		//$updated_log = $gn->update_logs($log->log_actions['content']);
		//$log->update($updated_log);		
		//$Gn->pre($log->toArray());
	}


}