<?php namespace App\Http\Controllers\gerbera;
use Excel;
use DateTime;

class generalController 
{

	public function pre($arr,$die = true){

		echo '<pre>';
		if(is_array($arr) || is_object($arr)){
			 print_r($arr);
		}else{
			echo $arr;
		}
		
		if($die) die;
	}

	public function ttosec($x){
		if(!$x) return 0;
		return strtotime($x) - strtotime('today');
	}

	public function sec_between_2_time($t1,$t2){
		$to = strtotime($t1);
		$fr = strtotime($t2);
		return abs($to - $fr);
	}

	public function jasonizer($old,$str,$type ='to'){
		if($type == 'to'):
			if($old == ''):
				$x = json_decode('[]',true);
			else:
				$x = json_decode($old,true);
			endif;
				$x[] = $str;
			return json_encode($x);
		endif;
	}
 
	public function jsonstrformatter($str){

		$x2 = [];

		if($str != ''):
			$x = json_decode($str);
			$last = '';
			foreach ($x as $v):
				$arr = explode('||', $v);
				if($arr[1] != $last):
					$x2[] = str_replace(["||"], " ", $v);
					$last = $arr[1];
				endif;
			endforeach;	
		endif;
		return $x2;	

	}


	public function userOptions_getter($json,$key){
		$result = '';
		if($json != '' && $json != '{}'):
			$options = json_decode($json,true);
			$result = $options[$key];
		endif;
		return $result;
	}

	public function userOptions_putter($json,$key,$value){

		if($json == ''): $json = '{}'; endif;
		$options = json_decode($json,true);
		$options[$key] = $value;
		return json_encode($options);

	}



	public function limit_text($text, $limit = 20){

      if (str_word_count($text, 0) > $limit) {
          $words = str_word_count($text, 2);
          $pos = array_keys($words);
          $text = substr($text, 0, $pos[$limit]) . '...';
      }

      return $text;

    }

    public function update_logs($data){

    	$carbon = new \Carbon\Carbon;
    	$a = json_decode($data,true);
    	$last = 'x';
    	$b = $c = $d = $c = $e = [];

        $logintemp = explode('||',$a[0]);
        $logintime = $logintemp[0];

    	foreach ($a as $k => $v):
    		$temp = explode('||', $v);
           
    		if($temp[1] != $last):
    			$b[] = $temp;
    		endif;
    		$last = $temp[1];
    	endforeach;

    	foreach ($b as $k => $v):
    			$n = $k + 1;
    			if(isset($b[$n])):
    				$v[2] = $this->sec_between_2_time($v[0],$b[$n][0]);
    			else:
    				$v[2] = $this->sec_between_2_time($v[0],$carbon->now()->format('Y-m-d H:i:s'));
    			endif;
    			$c[] = $v;
    	endforeach;

    	foreach ($c as $k => $v):

    		$key = trim($v[1]);
    		$val = trim($v[2]);

    		if(array_key_exists($key, $d)):
    			$d[$key] += $val;
    		else:
    			$d[$key] = $val;
    		endif;
    		
    	endforeach;

    	foreach ($d as $k => $v):
    		if(!in_array(trim($k),['logged in','logged out']))
    		$e[trim($k)] = gmdate("H:i:s", $v);
    	endforeach;
        $e['ago'] = $this->get_timeago(strtotime($logintime));
    	return $e;

    }

    public function format_title($str){
		return ucwords(str_replace(['_']," ",$str));
    }

    public function excel_exporter($arr,$filename){

    	$keys = [];
    	$data = [];

    	foreach ($arr as $a):
    		foreach ($a as $ak => $av):
    			if(!in_array($ak,$keys)):
    				if(!is_array($av)): 
    					$keys[] = $ak;
    				endif;
    			endif;
    		endforeach;
    	endforeach;

    	foreach ($arr as $ctr => $a):
    		foreach ($keys as $key):
    			$data[$ctr][$key] = (isset($a[$key])?$a[$key]:'');
    		endforeach;
    	endforeach;

    	$formatted_keys = [];
    	foreach ($keys as $k):
    		$formatted_keys[] = $this->format_title($k);
    	endforeach;

		Excel::create($filename, function($excel) use ($formatted_keys,$data) {
		    $excel->sheet('Sheet1', function($sheet) use ($formatted_keys,$data) {
					$sheet->row(1,$formatted_keys);
					$ctr = 2;
					foreach ($data as $k => $v):
							$sheet->row($ctr,$v);
							$ctr++;						
					endforeach;
		    });
		})->store('xls');

    }

    public function catcher($data,$type = 'int'){

    	$result = 'null';

    	switch ($type) {
    		case 'int':
    			$result = ($data?$data:0);
    			break;
    		case 'str':
    			$result = ($data?$data:'');
    			break;    			
    	}

    	return $result;

    }

    public function get_timeago($ptime){
        $etime = time() - $ptime;

        if( $etime < 1 )
        {
            return 'less than 1 second ago';
        }

        $a = array( 12 * 30 * 24 * 60 * 60  =>  'year',
                    30 * 24 * 60 * 60       =>  'month',
                    24 * 60 * 60            =>  'day',
                    60 * 60             =>  'hour',
                    60                  =>  'minute',
                    1                   =>  'second'
        );

        foreach( $a as $secs => $str )
        {
            $d = $etime / $secs;

            if( $d >= 1 )
            {
                $r = round( $d );
                return 'about ' . $r . ' ' . $str . ( $r > 1 ? 's' : '' ) . ' ago';
            }
        }
    } 

    public function hourspassed($t1,$t2){
        $date1 = new DateTime($t1);
        $date2 = new DateTime($t2);

        $diff = $date2->diff($date1);

        $hours = $diff->h;
        $hours = $hours + ($diff->days*24);
        return $hours;    
    }

    public function haveLogOut($row){

        $hours = $this->hourspassed($row->login,$row->logout);
        if($hours > 16):
            return 'failed to logout properly';
        else:
            return $row->logout;
        endif;

    } 

    public function getHoursDuty($row){
        $hours = $this->hourspassed($row->login,$row->logout);
        if($hours > 16):
            return '?';
        else:
            return $hours;
        endif;        
    }

    public function uriprefix($str){

        if($str != ''):
        $a = explode('/', $str);
        return $a[0];
        endif;
        return '';

    }

}
//http://quotes.rest/qod.json?category=inspire

