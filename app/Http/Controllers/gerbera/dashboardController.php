<?php namespace App\Http\Controllers\gerbera;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;


class dashboardController extends Controller
{

	 public function index(){ 

	 	if(Auth::user()->user_type == 'agent'):

				$gn = new \App\Http\Controllers\gerbera\generalController;
				$LogActions = new \App\Http\Models\gerbera\LogActions;
				$Campaigns = new \App\Http\Models\gerbera\Campaigns;
				$campaigns = $Campaigns->get(); 
				//$gn->pre($campaigns->toArray());
				$time = Carbon::now()->format('Y-m-d H:i:s');

				$Log = new \App\Http\Models\gerbera\Log;
				$log = $Log->active(Auth::user()->id)->first();
				//$log_id = empty($log->id) ? null : $log->id;

				if($log): 

					$log_actions = $LogActions->where('log_id',$log->id)->first();
					if(!$log_actions):
						$logtxt = $time.'|| '.$log->status;
						$log_actions = $LogActions->create([
							'log_id' => $log->id,
							'content' => $gn->jasonizer('',$logtxt),
							'created_at' => $time,
							'updated_at' => $time
						]);
					endif;

					$updated_log = $gn->update_logs($log_actions->content);
					$log->update($updated_log);


					foreach (['meeting','break','lunch','other'] as $v):
						if(isset($updated_log[$v])):
							$session[$v] = $updated_log[$v]; 
						else:
							$session[$v] = gmdate("H:i:s",0);
						endif;
					endforeach;
					//$gn->pre($session);die;
					$log = $log->toArray();
					//$gn->pre($log);die;
					return view('gerbera.timeout.loggedin',compact('sec_passed','session','log','gn','time','campaigns','log_id'));

				else:
					return view('gerbera.timeout.loggedout',compact('gn','campaigns'));
				endif;



	 	else:
	 		$Campaigns = new \App\Http\Models\gerbera\Campaigns;
			$campaigns = $Campaigns->select('id','name')->get();
	 		return view('gerbera.dashboard.index2',compact('campaigns'));
	 		
	 	endif;
	 	
	 }   


	public function filter(Request $r){ 

		
	 	if(Auth::user()->user_type == 'agent'):

				$gn = new \App\Http\Controllers\gerbera\generalController;
				$LogActions = new \App\Http\Models\gerbera\LogActions;
				$Campaigns = new \App\Http\Models\gerbera\Campaigns;
				$campaigns = $Campaigns->wherein('id',[$r['campaign']])->get();
				//$gn->pre($campaigns->toArray());
				$time = Carbon::now()->format('Y-m-d H:i:s');
				$Log = new \App\Http\Models\gerbera\Log;
				$log = $Log->active(Auth::user()->id)->first();

				if($log):

					$log_actions = $LogActions->where('log_id',$log->id)->first();
					if(!$log_actions):
						$logtxt = $time.'|| '.$log->status;
						$log_actions = $LogActions->create([
							'log_id' => $log->id,
							'content' => $gn->jasonizer('',$logtxt),
							'created_at' => $time,
							'updated_at' => $time
						]);
					endif;

					$updated_log = $gn->update_logs($log_actions->content);
					$log->update($updated_log);


					foreach (['meeting','break','lunch','other'] as $v):
						if(isset($updated_log[$v])):
							$session[$v] = $updated_log[$v]; 
						else:
							$session[$v] = gmdate("H:i:s",0);
						endif;
					endforeach;
					//$gn->pre($session);die;
					$log = $log->toArray();
					//$gn->pre($log);die;
					return view('gerbera.timeout.loggedin',compact('sec_passed','session','log','gn','time','campaigns'));

				else:
					return view('gerbera.timeout.loggedout',compact('gn','campaigns'));
				endif;



	 	else:

	 		$Campaigns = new \App\Http\Models\gerbera\Campaigns;
			$campaigns = $Campaigns->select('id','name')->get();

			$selected = $r['campaign'];
			$select = "";

			foreach ($selected as $key => $s) {

				if(!empty($select)):
					$select .= ",".$s ;
				else:
					$select .= $s;
				endif;
			}
			

	 		return view('gerbera.dashboard.filter',compact('campaigns','select'));
	 		
	 	endif;
	 	
	 }  






}
    