<?php namespace App\Http\Controllers\gerbera;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Response;
use Auth;
use Excel;
use Session;
use Storage;

class excelController extends Controller
{
	

	public function index(){
		$gn = new \App\Http\Controllers\gerbera\generalController;
		$List = new \App\Http\Models\gerbera\Lists;
		$lists = $List->orderBy('id','desc')->with(['user'])->paginate(20);
		return view('gerbera.excel.index',compact('lists'));
	}

	public function lists($id){

		$Data = new \App\Http\Models\gerbera\Data;
		$rows = $Data->where('list_id',$id)->paginate(50);
		$gn = new \App\Http\Controllers\gerbera\generalController;


    	$keys = [];
    	$data = [];

    	foreach ($rows as $row):
    		$a = json_decode($row->content,true);
    		foreach ($a as $ak => $av):
    			if(!in_array($ak,$keys)):
    				if(!is_array($av)):
    					$keys[] = $ak;
    				endif;
    			endif;
    		endforeach;
    	endforeach;

    	foreach ($rows as $ctr => $row):
    		$a = json_decode($row->content,true);
    		foreach ($keys as $key):
    			$data[$ctr][$key] = $gn->limit_text((isset($a[$key])?$a[$key]:''),5);
    		endforeach;
    	endforeach;

		return view('gerbera.excel.lists',compact('keys','data','rows'));

	}

	public function load(){

	 	return view('gerbera.excel.load');
	}

	public function unload(Request $r){

		$gn = new \App\Http\Controllers\gerbera\generalController;
		$List = new \App\Http\Models\gerbera\Lists;
		$File = new \App\Http\Models\gerbera\Files;
		$Carbon = new \Carbon\Carbon;
		$now = $Carbon->now()->format('Y-m-d H:i:s');
		$lists = $List->where('id',$r->list_id)->with(['data'])->first()->toArray();
		$new_arr = [];
		//$gn->pre($lists);
		foreach($lists['data'] as $list):
			$edited = 'no';
			$agent = '';
			if($list['done']):
				$data = $list['done'];
				$agent = $list['done']['user']['name'];
				$edited = 'yes';
			else:
				$data = $list;
			endif;

			$a = [
				'edited' => $edited,
				'agent' => $agent,
				'status' => $data['status']
			];
			$b = json_decode($data['content'],true);
			$new_arr[] = array_merge($b, $a);
		endforeach;
		$filename = str_replace([' ','-'],'_',$lists['name'].date('YmdHis'));
		$gn->excel_exporter($new_arr,$filename);
		$File->create(['name' => $lists['name'],'filename' => $filename,'user_id' => Auth::user()->id, 'created_at' => $now, 'updated_at' => $now]);
		
		$Del = $List->where('id',$r->list_id)->with('data')->first();
		$Del->data()->delete();
		$Del->meta()->update(['status' => 'removed']);
		$Del->delete();
		
		return redirect()->back();
		
	}



	 public function post(Request $r){

	 	$gn = new \App\Http\Controllers\gerbera\generalController;
	 	$List = new \App\Http\Models\gerbera\Lists;
	 	$title = ($r->title?$r->title:'untitled');
	 	$userid = Auth::user()->id;
	 	$now = Carbon::now()->format('Y-m-d H:i:s');
	 	$list = $List->create(['name' => $title,'user_id' => $userid,'created_at' => $now,'updated_at' => $now]);
		$x = Excel::load($r->file('excelfile'), function($reader) {})->get();
		//$gn->pre($x->toArray());
		if($x): 
			$rows = $x->toArray();
			if($rows):
				$Carbon= new Carbon; 
				$now = $Carbon->now()->format('Y-m-d H:i:s');

			    $arr = [];
			    $count = 0;
				foreach($rows as $row):
					$arr[] = [
						'content' => json_encode($row),
						'status' => 0,
						'list_id' => $list->id,
						'created_at' => $now,
						'updated_at' => $now
					];
					$count++;
					if($count == 100):
						$Data = new \App\Http\Models\gerbera\Data;
						$Data->insert($arr);
						$arr = [];
						$count = 0;
					endif;
				endforeach;
				if($arr):
					$Data = new \App\Http\Models\gerbera\Data;
					$Data->insert($arr);
				endif;
			endif;
		endif; 

		Session::flash('status', json_encode(['status'=>1,'msg' => 'uploaded successfully']));
		return redirect()->back();
		
	 }

	 public function archive(){

	 	$Files = new \App\Http\Models\gerbera\Files;

	 	$rows = $Files->paginate(40);
	 	return view('gerbera.excel.files',compact('rows'));

	 }

	 public function download($filename){

	    return  Response::download(storage_path().'\exports\\'.$filename.'.xls');

	 }

}