<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        return view('home');
    }

    public function timecheck()
    {

        $manila_time = date("m-d-Y H:i");

        date_default_timezone_set('America/Los_Angeles');

        $pst_time = date('m-d-Y H:i');

        return view('timecheck',compact('pst_time','manila_time'));
    }
}
