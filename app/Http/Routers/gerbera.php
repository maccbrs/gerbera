<?php 

$this->group(['middleware' => ['web','auth','timekeep'],'namespace' => 'gerbera','middleware' => ['gerbera-auth']],function(){ 

	$this->group(['prefix' => 'dashboard'],function(){
		$this->get('/',['as' => 'gerbera.dashboard.index','uses' => 'dashboardController@index']);
		$this->post('/filter',['as' => 'gerbera.dashboard.filter','uses' => 'dashboardController@filter']);
	});

	$this->group(['prefix' => 'status'],function(){
		$this->post('/timein',['as' => 'gerbera.status.timein','uses' => 'statusController@timein']);
		$this->post('/timeout',['as' => 'gerbera.status.timeout','uses' => 'statusController@timeout']);
		$this->post('/update',['as' => 'gerbera.status.update','uses' => 'statusController@update']);
		$this->post('/change',['as' => 'gerbera.status.change','uses' => 'statusController@change']);
	});


	$this->group(['prefix' => 'user','middleware' => ['plum.auth.admin']],function(){
		$this->get('/',['as' => 'gerbera.user.index','uses' => 'userController@index']);
		$this->post('/destroy/{id}',['as' => 'gerbera.user.destroy','uses' => 'userController@destroy']);
		$this->post('/reset_pass/{id}',['as' => 'gerbera.user.reset_pass','uses' => 'userController@reset_pass']);
		$this->post('/update/{id}',['as' => 'gerbera.user.update','uses' => 'userController@update']);
		$this->post('/create',['as' => 'gerbera.user.create','uses' => 'userController@create']);
	});		

	$this->group(['prefix' => 'excel','middleware' => ['plum.auth.ops']],function(){

		$this->get('/index',['as' => 'gerbera.excel.index','uses' => 'excelController@index']);
		$this->get('/lists/{id}',['as' => 'gerbera.excel.lists','uses' => 'excelController@lists']);
		$this->get('/load',['as' => 'gerbera.excel.load','uses' => 'excelController@load']);
		$this->post('/load',['as' => 'gerbera.excel.post','uses' => 'excelController@post']);
		$this->post('/unload',['as' => 'gerbera.excel.unload','uses' => 'excelController@unload']);
		$this->get('/archive',['as' => 'gerbera.excel.archive','uses' => 'excelController@archive']);
		$this->get('/download/{filename}',['as' => 'gerbera.excel.download','uses' => 'excelController@download']);

	});	

	$this->group(['prefix' => 'data'],function(){
		$this->post('/update/{id}',['as' => 'gerbera.data.update','uses' => 'dataController@update']);
	});

	$this->group(['prefix' => 'timeout'],function(){
		$this->get('/pause',['as' => 'gerbera.timeout.pause','uses' => 'timeoutController@pause']);
		$this->post('/back-to-work',['as' => 'gerbera.timeout.backtowork','uses' => 'timeoutController@back_to_work']);
	});

	$this->group(['prefix' => 'reports','middleware' => ['plum.auth.ops']],function(){
		$this->get('/generate',['as' => 'gerbera.reports.generate','uses' => 'reportsController@generate']);
		$this->get('/account',['as' => 'gerbera.reports.account','uses' => 'reportsController@account']);
		$this->post('/generate',['as' => 'gerbera.reports.generate_post','uses' => 'reportsController@generate_post']);
		$this->post('/generate_account',['as' => 'gerbera.reports.generate_account','uses' => 'reportsController@generate_account']);
		$this->get('/agentlog',['as' => 'gerbera.reports.agentlog','uses' => 'reportsController@agent_log']);
		$this->post('/generate_agent',['as' => 'gerbera.reports.generate_agent','uses' => 'reportsController@generate_agent']);
	});
	$this->group(['prefix' => 'reports'],function(){
		$this->get('/agentrecord',['as' => 'gerbera.reports.agentrecord','uses' => 'reportsController@agentrecord']);
	});


	$this->group(['prefix' => 'monitoring','middleware' => ['plum.auth.ops']],function(){
		$this->post('/agent',['as' => 'gerbera.monitoring.agent','uses' => 'monitoringController@agent']);
		$this->post('/filter',['as' => 'gerbera.monitoring.filter','uses' => 'monitoringController@filter']);
		$this->get('/test',['as' => 'gerbera.monitoring.test','uses' => 'monitoringController@test']);
	});

	$this->group(['prefix' => 'campaigns'],function(){
		$this->get('/',['as' => 'gerbera.campaigns.index','uses' => 'campaignsController@index']);
		$this->post('/create',['as' => 'gerbera.campaigns.create','uses' => 'campaignsController@create']);
		$this->post('/destroy/{id}',['as' => 'gerbera.campaigns.destroy','uses' => 'campaignsController@destroy']);
		$this->post('/switch',['as' => 'gerbera.campaign.switch','uses' => 'campaignsController@switchCampaign']);
		
	});

	$this->group(['prefix' => 'test'],function(){
		$this->get('/',['as' => 'gerbera.test.index','uses' => 'testController@index']);

	});

	$this->group(['prefix' => 'redis'],function(){
		$this->get('/',['as' => 'gerbera.redis.index','uses' => 'redisController@index']);

	});

	$this->group(['prefix' => 'timekeeper'],function(){
		$this->get('/',['as' => 'gerbera.timekeeper.index','uses' => 'timekeeperController@index']);
		$this->post('/run',['as' => 'gerbera.timekeeper.run','uses' => 'timekeeperController@run']);
		$this->post('/stop',['as' => 'gerbera.timekeeper.stop','uses' => 'timekeeperController@stop']);
		$this->post('/switch',['as' => 'gerbera.timekeeper.switch','uses' => 'timekeeperController@switchs']);
		$this->post('/timeout',['as' => 'gerbera.timekeeper.timeout','uses' => 'timekeeperController@timeout']);
		//$this->get('/forcetimeout/{logid}',['as' => 'gerbera.timekeeper.forcetimeout','uses' => 'timekeeperController@forcelogout']);
		$this->post('/timein',['as' => 'gerbera.timekeeper.timein','uses' => 'timekeeperController@timein']);
	});	
	
	
	$this->group(['prefix' => 'forcetimeout'],function(){
		
		
		$this->get('/{logid}',['as' => 'gerbera.forcetimeout','uses' => 'timekeeperController@forcelogout']);
		
	});	
}); 
