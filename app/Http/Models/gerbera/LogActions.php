<?php namespace App\Http\Models\gerbera;

use Illuminate\Database\Eloquent\Model;

class LogActions extends Model
{
   protected $connection = 'gerbera';
   protected $table = 'log_actions';
   protected $fillable = ['log_id','content','created_at','updated_at'];



}