<?php namespace App\Http\Models\gerbera;

use Illuminate\Database\Eloquent\Model;


class Log extends Model
{
   protected $connection = 'gerbera';
   protected $table = 'log';

   protected $fillable = ['user_id','account_id','campaign_id','status','break','ago','meeting','lunch','other','login','logout','created_at','updated_at','last_updated'];


	public function scopeActive($query,$userid)
	{
	    return $query->where('user_id',$userid)->where('status', '!=', 'logged out')->orderBy('login','desc');
	}

  public function scopeAuth($query,$param){
    return $query->where('user_id',$param)->where('status','!=','logged out')->orderBy('login','desc');
  }

  public function scopeLast($query,$param){
  	return $query->where('user',$param)->orderBy('login','desc');
  }

  public function logs(){ 
    return $this->hasOne('App\Http\Models\gerbera\AccountLog','log_id');       
  }	

  public function campaign(){ 
    return $this->hasOne('App\Http\Models\gerbera\Campaigns','id','campaign_id');       
  } 

  public function user(){
    return $this->belongsTo('App\Http\Models\gerbera\User','user_id');       
  } 

  public function metadata(){
    return $this->hasMany('App\Http\Models\gerbera\MetaData','log_id');       
  } 

  public function log_actions(){
    return $this->hasOne('App\Http\Models\gerbera\LogActions','log_id');       
  } 

  public function log_footprints(){
    return $this->hasOne('App\Http\Models\gerbera\LogFootprints','log_id');       
  } 

}