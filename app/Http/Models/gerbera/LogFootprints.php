<?php namespace App\Http\Models\gerbera;

use Illuminate\Database\Eloquent\Model;

class LogFootprints extends Model
{
   protected $connection = 'gerbera';
   protected $table = 'log_footprints';
   protected $fillable = ['log_id','content','created_at','updated_at'];



}