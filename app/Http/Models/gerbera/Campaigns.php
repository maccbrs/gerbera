<?php namespace App\Http\Models\gerbera;

use Illuminate\Database\Eloquent\Model;

class Campaigns extends Model
{
   protected $connection = 'gerbera';
   protected $table = 'campaigns';

   protected $fillable = ['campaign_id','name','created_at','updated_at'];



}