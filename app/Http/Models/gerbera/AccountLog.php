<?php namespace App\Http\Models\gerbera;

use Illuminate\Database\Eloquent\Model;

class AccountLog extends Model
{
   protected $connection = 'gerbera';
   protected $table = 'account_log';

   protected $fillable = ['log_id','login','logout','account_id','created_at','updated_at'];



}