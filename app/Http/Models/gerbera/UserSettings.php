<?php namespace App\Http\Models\gerbera;

use Illuminate\Database\Eloquent\Model;

class UserSettings extends Model
{
   protected $connection = 'gerbera';
   protected $table = 'user_settings';
   protected $fillable = ['user_id','user_type','password','created_at','updated_at'];



}