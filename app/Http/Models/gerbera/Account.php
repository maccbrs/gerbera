<?php namespace App\Http\Models\gerbera;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
   protected $connection = 'gerbera';
   protected $table = 'account';

   protected $fillable = ['name','status','account_id','created_at','updated_at'];



}