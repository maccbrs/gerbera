<?php namespace App\Http\Models\gerbera;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $connection = 'gerbera';
    protected $table = 'users';   
    protected $fillable = ['name', 'email', 'password','user_type'];
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function settings(){
      return $this->hasOne('App\Http\Models\gerbera\UserSettings','user_id');       
    }    

    public function activelog(){
      return $this->hasOne('App\Http\Models\gerbera\Log','user_id')->where('status','!=','logged out')->orderBy('created_at','desc');       
    }  





}
